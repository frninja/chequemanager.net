﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Entities
{
    public class CustomPrice
    {
        private decimal? _price;

        public decimal Price 
        { 
            get 
            { 
                return _price != null ? (decimal)_price : 0; 
            }
            private set
            {
                _price = value;
            }
        }

        public CustomPrice(decimal? price)
        {
            _price = price;
        }
    }
}
