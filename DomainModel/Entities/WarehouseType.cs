﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Entities
{
    public enum WarehouseTypes { RETAIL, WHOLESALE, UNKNOWN };

    public class WarehouseType 
    {
        public WarehouseTypes Type {get; private set;}

        public WarehouseType(WarehouseTypes type)
        {
            Type = type;
        }
    }
}
