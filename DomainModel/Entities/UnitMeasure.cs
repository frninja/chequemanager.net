﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Entities
{
    public class UnitMeasure
    {
        public string UnitOfMeasure { get; set; }

        public UnitMeasure(string unitOfMeasure)
        {
            UnitOfMeasure = unitOfMeasure;
        }
    }
}
