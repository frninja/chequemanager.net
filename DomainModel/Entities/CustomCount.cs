﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Exceptions;

namespace DomainModel.Entities
{
    public class CustomCount
    {
        public decimal Count { get; set; }
        
        public CustomCount(decimal count)
        {
            if (count >= 0)
            {
                Count = count;
            }
            else
            {
                throw new InvalidCustomCountException("Must be greater or equal zero.");
            }
        }
    }
}
