﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Entities
{
    public class Town
    {
        public string Name { get; set; }
        
        public Town(string name)
        {
            Name = name;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
