﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Entities
{
    public class Seller
    {
        public SellerId ID { get; set; }
        public Name Name { get; set; }
        public Town Town { get; set; }
        public PhoneNumber Phone { get; set; }
        public SellerFiredState FiredState { get; set; }

        public Seller(SellerId id, Name name, Town town, PhoneNumber number, SellerFiredState state)
        {
            ID = id;
            Name = name;
            Town = town;
            Phone = number;
            FiredState = state;
        }
    }
}
