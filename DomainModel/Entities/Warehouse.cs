﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Entities
{
    public class Warehouse
    {
        public WarehouseId ID { get; set; }
        public WarehouseName Name { get; set; }
        public Town Town { get; set; }
        public WarehouseType Type { get; set; }

        public Warehouse(WarehouseId id, WarehouseName name, Town town, WarehouseType type)
        {
            ID = id;
            Name = name;
            Town = town;
            Type = type;
        }
    }
}
