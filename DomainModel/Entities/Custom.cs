﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DomainModel.Entities
{
    public class Custom
    {
        public CustomId ID { get; set; }
        public CustomNomenclature Nomenclature { get; set; }
        public UnitMeasure UnitOfMeasure { get; set; }

        public CustomPrice Price { get; set; }

        public Custom(CustomId id, CustomNomenclature nomenclature, CustomPrice price, UnitMeasure unitOfMeasure)
        {
            ID = id;
            Nomenclature = nomenclature;
            UnitOfMeasure = unitOfMeasure;
            Price = price;
        }
    }
}
