﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Entities
{
    public class CustomNomenclature
    {
        public string Nomeclature { get; private set; }
        
        public CustomNomenclature(string nomenclature)
        {
            Nomeclature = nomenclature;
        }

        public override string ToString()
        {
            return Nomeclature;
        }
    }
}
