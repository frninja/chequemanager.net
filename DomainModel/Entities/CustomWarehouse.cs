﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Entities
{
    public class CustomWarehouse
    {
        public CustomWarehouseId Id { get; set; }
        public WarehouseId WarehouseId { get; set; }
        public CustomId CustomId { get; set; }
        public CustomCount Count { get; set; }

        public CustomWarehouse(CustomWarehouseId customWarehouseId, WarehouseId warehouseId, CustomId customId, CustomCount count)
        {
            Id = customWarehouseId;
            WarehouseId = warehouseId;
            CustomId = customId;
            Count = count;
        }
    }
}
