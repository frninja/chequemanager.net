﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Entities
{
    public class Name
    {
        public string FullName { get; private set; }

        public Name(string fullName)
        {
            FullName = fullName;
        }

        public override string ToString()
        {
            return FullName;
        }
    }
}
