﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics.Contracts;

namespace DomainModel.Entities
{
    public class SellerBonus
    {
        public decimal Bonus { get; set; }

        public SellerBonus(decimal bonus)
        {
            Contract.Requires(bonus >= 0);

            Bonus = bonus;
        }
    }
}
