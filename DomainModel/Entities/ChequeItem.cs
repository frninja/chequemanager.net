﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DomainModel.Entities
{
    public class ChequeItem
    {
        public ChequeItemId ID { get; set; }
        public ChequeId ChequeID { get; set; }

        public CustomId CustomID { get; set; }

        public CustomCount CustomCount { get; set; }

        public WarehouseId WarehouseID { get; set; }

        public ChequeItem(ChequeItemId id,
            ChequeId chequeId,
            CustomId customId,
            CustomCount customCount,
            WarehouseId warehouseId)
        {
            ID = id;
            ChequeID = chequeId;
            CustomID = customId;
            CustomCount = customCount;
            WarehouseID = warehouseId;
        }
    }
}
