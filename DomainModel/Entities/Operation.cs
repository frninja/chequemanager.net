﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Entities
{
    public class Operation
    {
        public OperationId ID { get; set; }
        public CustomId CustomID { get; set; }
    }
}
