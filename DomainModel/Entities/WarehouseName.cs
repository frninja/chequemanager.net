﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Entities
{
    public class WarehouseName
    {
        public string Name { get; private set; }

        public WarehouseName(string name)
        {
            Name = name;
        }
    }
}
