﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Entities
{
    public class CountedCustom
    {
        public Custom Custom { get; private set; }
        public CustomCount Count { get; private set; }

        public CountedCustom(Custom custom, CustomCount count)
        {
            Custom = custom;
            Count = count;
        }
    }
}
