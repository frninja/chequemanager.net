﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Exceptions
{
    public class InvalidCustomCountException : Exception
    {
        public InvalidCustomCountException(string message) : base(message)
        {
        }

        public InvalidCustomCountException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
