﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;
using DataAccessLayer.Gateway;

namespace BusinessLogicLayer.Commands
{
    public class CommandInvoker : ICommandInvoker
    {
        private readonly IDatabaseGateway _databaseGateway;
        private readonly ISellerGateway _sellerGateway;
        private readonly ICustomGateway _customGateway;
        private readonly IChequeGateway _chequeGateway;
        private readonly IWarehouseGateway _warehouseGateway;
        private readonly ICustomWarehouseGateway _customWarehouseGateway;

        public CommandInvoker(IDatabaseGateway databaseGateway,
            ISellerGateway sellerGateway,
            IWarehouseGateway warehouseGateway,
            ICustomGateway customGateway,
            IChequeGateway chequeGateay,
            ICustomWarehouseGateway customWarehouseGateway)
        {
            _databaseGateway = databaseGateway;
            _sellerGateway = sellerGateway;
            _warehouseGateway = warehouseGateway;
            _customGateway = customGateway;
            _chequeGateway = chequeGateay;
            _customWarehouseGateway = customWarehouseGateway;
        }

        public virtual void AddSeller(Seller seller)
        {
            try
            {
                var cmd = new AddSellerCommand(_sellerGateway, seller);
                cmd.Execute();
            }
            catch (Exception e)
            {
                throw new CommandInvokerException(this.GenerateExceptionText("Add seller command failed.", e), e);
            }
        }

        public virtual void FireSeller(Seller seller)
        {
            try
            {
                var cmd = new FireSellerCommand(_sellerGateway, seller);
                cmd.Execute();
            }
            catch (Exception e)
            {
                throw new CommandInvokerException(this.GenerateExceptionText("Fire seller command failed.", e), e);
            }
        }

        public virtual SellerBonus CalculateSellerBonus(Seller seller)
        {
            try
            {
                var cmd = new CalculateSellerBonusCommand(_sellerGateway, seller);
                return cmd.Execute();
            }
            catch (Exception e)
            {
                throw new CommandInvokerException(this.GenerateExceptionText("Calculate seller bonus command failed.", e), e);
            }
        }

        public virtual IEnumerable<Seller> GetAllSellers()
        {
            try
            {
                var cmd = new GetAllSellersCommand(_sellerGateway);
                return cmd.Execute();
            }
            catch (Exception e)
            {
                throw new CommandInvokerException(this.GenerateExceptionText("Get all sellers command failed.", e), e);
            }
        }

        public virtual IEnumerable<Seller> GetWorkingSellers()
        {
            try
            {
                var cmd = new GetWorkingSellersCommand(_sellerGateway);
                return cmd.Execute();
            }
            catch (Exception e)
            {
                throw new CommandInvokerException(this.GenerateExceptionText("Get working sellers command failed.", e), e);
            }
        }

        public virtual IEnumerable<Warehouse> GetAllWarehouses()
        {
            try
            {
                var cmd = new GetAllWarehousesCommand(_warehouseGateway);
                return cmd.Execute();
            }
            catch (Exception e)
            {
                throw new CommandInvokerException(this.GenerateExceptionText("Get all warehouses command failed.", e), e);
            }
        }
        
        public virtual IEnumerable<Warehouse> GetRetailWarehouses()
        {
            try
            {
                var cmd = new GetRetailWarehousesCommand(_warehouseGateway);
                return cmd.Execute();
            }
            catch (Exception e)
            {
                throw new CommandInvokerException(this.GenerateExceptionText("Get retail warehouses command failed.", e), e);
            }
        }

        public virtual IEnumerable<Custom> GetAllCustoms()
        {
            try
            {
                var cmd = new GetAllCustomsCommand(_customGateway);
                return cmd.Execute();
            }
            catch (Exception e)
            {
                throw new CommandInvokerException(this.GenerateExceptionText("Get all customs command failed.", e), e);
            }
        }

        public virtual IEnumerable<CountedCustom> GetAllCustoms(Warehouse warehouse)
        {
            try
            {
                var cmd = new GetWarehouseCustomsCountCommand(_customWarehouseGateway, warehouse);
                return cmd.Execute();
            }
            catch (Exception e)
            {
                throw new CommandInvokerException(this.GenerateExceptionText("Get all customs command failed.", e), e);
            }
        }

        public virtual IEnumerable<Cheque> GetAllCheques()
        {
            try
            {
                var cmd = new GetAllChequesCommand(_chequeGateway);
                return cmd.Execute();
            }
            catch (Exception e)
            {
                throw new CommandInvokerException(this.GenerateExceptionText("Get all cheques command failed.", e), e);
            }
        }

        public virtual IEnumerable<ChequeItem> GetChequeItems(Cheque cheque)
        {
            try
            {
                var cmd = new GetChequeItemsCommand(_chequeGateway, cheque);
                return cmd.Execute();
            }
            catch (Exception e)
            {
                throw new CommandInvokerException(this.GenerateExceptionText("Get cheque items command failed.", e), e);
            }
        }

        public virtual void CreateCheque(SellerId sellerId, IEnumerable<ChequeItem> items)
        {
            try
            {
                _databaseGateway.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);

                var cmd = new CreateChequeCommand(_chequeGateway, sellerId, items);

                try
                {
                    cmd.Execute();
                    _databaseGateway.CommitActiveTransaction();
                }
                catch 
                {
                    _databaseGateway.RollbackActiveTransaction();
                    throw;
                }
            }
            catch (Exception e)
            {
                throw new CommandInvokerException(this.GenerateExceptionText("Create cheque command failed.", e), e);
            }
            
        }

        public virtual void CancelCheque(Cheque cheque)
        {
            try
            {
                var cmd = new CancelChequeCommand(_chequeGateway, cheque);
                cmd.Execute();
            }
            catch (Exception e)
            {
                throw new CommandInvokerException(this.GenerateExceptionText("Cancel cheque command failed.", e), e);
            }
        }

        private string GenerateExceptionText(string text, Exception e)
        {
            return new StringBuilder()
                    .Append(text)
                    .AppendLine()
                    .Append(e.Message)
                    .ToString();
        }
    }
}
