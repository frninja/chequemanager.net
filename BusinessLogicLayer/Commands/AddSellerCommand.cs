﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics.Contracts;

using DomainModel.Entities;
using DataAccessLayer.Gateway;

namespace BusinessLogicLayer.Commands
{
    public class AddSellerCommand : Command, ICommand
    {
        private readonly ISellerGateway _sellerGateway;

        private readonly Seller _seller;
        
        public AddSellerCommand(ISellerGateway gateway, Seller seller)
        {
            Contract.Requires(gateway != null);
            Contract.Requires(seller != null);

            _sellerGateway = gateway;
            _seller = seller;
        }

        public override void Execute()
        {
            _sellerGateway.AddSeller(_seller);
        }
    }
}
