﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;
using DataAccessLayer.Gateway;

namespace BusinessLogicLayer.Commands
{
    public class GetAllCustomsCommand : Command<IEnumerable<Custom>>
    {
        private readonly ICustomGateway _customGateway;

        public GetAllCustomsCommand(ICustomGateway gateway)
        {
            _customGateway = gateway;
        }

        public override IEnumerable<Custom> Execute()
        {
            return _customGateway.GetAllCustoms();
        }
    }
}
