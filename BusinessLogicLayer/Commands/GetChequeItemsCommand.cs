﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics.Contracts;

using DomainModel.Entities;
using DataAccessLayer.Gateway;

namespace BusinessLogicLayer.Commands
{
    public class GetChequeItemsCommand : Command<IEnumerable<ChequeItem>>, ICommand<IEnumerable<ChequeItem>>
    {
        private readonly IChequeGateway _chequeGateway;
        private readonly Cheque _cheque;

        public GetChequeItemsCommand(IChequeGateway gateway, Cheque cheque)
        {
            Contract.Requires(gateway != null);
            Contract.Requires(cheque != null);

            _chequeGateway = gateway;
            _cheque = cheque;
        }

        public override IEnumerable<ChequeItem> Execute()
        {
            return _chequeGateway.GetChequeItems(_cheque);
        }
    }
}
