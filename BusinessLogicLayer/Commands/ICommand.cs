﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Commands
{
    public interface ICommand
    {
        void Execute();
    }

    public interface ICommand<TResult>
    {
        TResult Execute();
    }
}
