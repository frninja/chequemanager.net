﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;
using DataAccessLayer.Gateway;

using System.Data;
using System.Diagnostics.Contracts;

namespace BusinessLogicLayer.Commands
{
    public class CreateChequeCommand : Command
    {
        private readonly IChequeGateway _chequeGateway;

        private SellerId SellerID { get; set; }
        private IEnumerable<ChequeItem> Items { get; set; }


        public CreateChequeCommand(IChequeGateway gateway, SellerId id, IEnumerable<ChequeItem> items)
        {
            Contract.Requires(SellerID != null);
            Contract.Requires(Items != null);

            _chequeGateway = gateway;
            SellerID = id;
            Items = items;
        }

        public override void Execute()
        {
            ChequeId id = _chequeGateway.CreateCheque(SellerID);
            _chequeGateway.AddChequeItems(id, Items);
        }
    }
}
