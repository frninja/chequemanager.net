﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;
using DataAccessLayer.Gateway;

namespace BusinessLogicLayer.Commands
{
    public class GetAllWarehousesCommand : Command<IEnumerable<Warehouse>>
    {
        private readonly IWarehouseGateway _warehouseGateway;

        public GetAllWarehousesCommand(IWarehouseGateway gateway)
        {
            _warehouseGateway = gateway;
        }

        public override IEnumerable<Warehouse> Execute()
        {
            return _warehouseGateway.GetAllWarehouses();
        }
    }
}
