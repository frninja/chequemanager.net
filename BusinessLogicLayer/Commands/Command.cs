﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Commands
{
    public abstract class Command : ICommand
    {
        public abstract void Execute();
    }

    public abstract class Command<TResult> : ICommand<TResult>
    {
        public abstract TResult Execute();
    }
}
