﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataAccessLayer.Gateway;
using DomainModel.Entities;

namespace BusinessLogicLayer.Commands
{
    public class GetRetailWarehousesCommand : Command<IEnumerable<Warehouse>>
    {
        private readonly IWarehouseGateway _warehouseGateway;

        public GetRetailWarehousesCommand(IWarehouseGateway gateway)
        {
            _warehouseGateway = gateway;
        }

        public override IEnumerable<Warehouse> Execute()
        {
            return _warehouseGateway.GetRetailWarehouses();
        }
    }
}
