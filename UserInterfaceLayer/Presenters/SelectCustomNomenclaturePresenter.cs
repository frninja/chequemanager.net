﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;
using DataAccessLayer.Gateway;

using UserInterfaceLayer.Views;
using UserInterfaceLayer.Events;

using ApplicationServicesLayer;
using BusinessLogicLayer.Commands;

namespace UserInterfaceLayer.Presenters
{

    public class SelectCustomNomenclaturePresenter : BasePresenter<ISelectCustomNomenclatureView>, ISelectCustomNomenclaturePresenter
    {
        private List<Custom> _customs = new List<Custom>();

        public Warehouse CurrentWarehouse { get; set; }

        public SelectCustomNomenclaturePresenter(IApplicationController controller, ISelectCustomNomenclatureView view) : base(controller, view)
        {
            View.Closing += OnClosing;
            View.Loaded += OnLoaded;
            View.CustomNomenclatureSelected += OnCustomNomenclatureSelected;
        }

        public void Initialize(Warehouse warehouse)
        {
            CurrentWarehouse = warehouse;
            _customs.Clear();
            // Update customs
            try
            {
                _customs.AddRange(Controller.GetInstance<ICommandInvoker>().GetAllCustoms(CurrentWarehouse)
                    .Where(x => x.Count.Count > 0)
                    .Select(x => x.Custom));
                View.LoadCustoms(_customs.Select(x => x.Nomenclature.Nomeclature));
            }
            catch (Exception e)
            {
                View.ShowError(e.Message, "Error");
            }
        }

        private void OnClosing()
        {
            ISellPresenter presenter = Controller.GetInstance<ISellPresenter>();
            presenter.Show();
            presenter.Activate();
            View.Hide();
        }

        private void OnLoaded()
        {
            /*try
            {
                _customs = Controller.GetInstance<ICommandInvoker>().GetAllCustoms(CurrentWarehouse)
                    .Where(x => x.Count.Count > 0)
                    .Select(x => x.Custom).ToArray();
                View.LoadCustoms(_customs);
            }
            catch (Exception e)
            {
                View.ShowError(e.Message, "Error");
            }*/
            
        }

        private void OnCustomNomenclatureSelected(object sender, CustomNomenclatureSelectedEventArgs e)
        {
            if (e.CustomIndex != -1)
            {
                ISetCustomCountPresenter presenter = Controller.GetInstance<ISetCustomCountPresenter>();
                //presenter.Custom = _customs[e.CustomIndex];
                presenter.Initialize(_customs[e.CustomIndex]);
                presenter.Show();
                presenter.Activate();
            }
            else
            {
                View.ShowError("Custom is not selected", "Error");
            }
        }
    }
}
