﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ApplicationServicesLayer;
using UserInterfaceLayer.Events;
using UserInterfaceLayer.Views;

using DomainModel.Entities;
using BusinessLogicLayer.Commands;

namespace UserInterfaceLayer.Presenters
{
    public class ChequeViewPresenter : BasePresenter<IChequeView>, IChequeViewPresenter
    {
        private Cheque _cheque;
        private readonly List<ChequeItem> _chequeItems = new List<ChequeItem>();
        private readonly List<Custom> _customs = new List<Custom>();
        private readonly List<Warehouse> _warehouses = new List<Warehouse>();

        public event EventHandler<ChequeCancellingEventArgs> ChequeCancelling;

        public ChequeViewPresenter(IApplicationController controller, IChequeView view) : base(controller, view)
        {
            View.Closing += OnClosing;
            View.ChequeCancelPressed += OnChequeCancelPressed;
            View.ChequeCancelling += OnChequeCancelling;

            Controller.GetInstance<IEventAggregator>().Listen(this);
            Controller.GetInstance<IEventAggregator>().ChequeCancelled += OnChequeCancelled;
        }

        public void Initialize(Cheque cheque)
        {
            _cheque = cheque;

            _chequeItems.Clear();
            _customs.Clear();
            _warehouses.Clear();

            View.ClearChequeItems();

            // Load cheque here!
            try 
            {
                _chequeItems.AddRange(Controller.GetInstance<ICommandInvoker>().GetChequeItems(_cheque));
                _customs.AddRange(Controller.GetInstance<ICommandInvoker>().GetAllCustoms());
                _warehouses.AddRange(Controller.GetInstance<ICommandInvoker>().GetAllWarehouses());

                this.LoadChequeItems();
                View.EnableCancelling(!_cheque.CancellationState.IsCancelled);
            }
            catch (Exception ex)
            {
                View.EnableCancelling(false);
                View.ShowError(ex.Message, "Failed to load cheque items!");
            }

        }

        private void LoadChequeItems()
        {
            foreach (ChequeItem item in _chequeItems)
            {
                Custom custom = _customs.Find(c => c.ID.Id == item.CustomID.Id);
                //string custom = _customs.Find(c => c.ID.Id == item.CustomID.Id).Nomenclature.Nomeclature;
                string warehouse = _warehouses.Find(w => w.ID.Id == item.WarehouseID.Id).Name.Name;
                View.AddChequeItem(custom.Nomenclature.Nomeclature,
                    warehouse,
                    custom.Price.Price.ToString(),
                    item.CustomCount.Count.ToString(),
                    (custom.Price.Price * item.CustomCount.Count).ToString());
            }
        }

        private void OnClosing()
        {
            View.Hide();
        }

        private void OnChequeCancelPressed()
        {
            View.ShowCancellingDialog();
        }

        private void OnChequeCancelling(object sender, ChequeCancellingEventArgs e)
        {
            if (ChequeCancelling != null)
            {
                ChequeCancelling(this, new ChequeCancellingEventArgs(_cheque));
            }
        }

        private void OnChequeCancelled(object sender, ChequeCancelledEventArgs e)
        {
            if (_cheque.ID.Id == e.Cheque.ID.Id)
            {
                View.EnableCancelling(false);
            }
        }
    }
}
