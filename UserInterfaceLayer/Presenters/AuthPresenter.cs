﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UserInterfaceLayer.Views;

using ApplicationServicesLayer;

using UserInterfaceLayer.Events;

namespace UserInterfaceLayer.Presenters
{
    public class AuthPresenter : BasePresenter<IAuthView>, IAuthPresenter
    {
        public AuthPresenter(IApplicationController controller, IAuthView view) : base(controller, view)
        {
            View.LoggingIn += OnLoggingIn;
        }

        private void OnLoggingIn(object sender, LoggingInEventArgs e)
        {
            switch (e.AuthType)
            {
                case AuthType.MANAGER:
                    {
                        Console.WriteLine("Login as manager");
                        IManagerPresenter presenter = Controller.GetInstance<IManagerPresenter>();
                        presenter.Initialize();
                        presenter.Show();
                        presenter.Activate();
                        View.Hide();
                        //View.ShowError("Manager interface is available only in paid version.", "Internal error!");
                        break;
                    }
                case AuthType.SELLER:
                    {
                        Console.WriteLine("Login as seller");
                        ISelectSellerPresenter presenter = Controller.GetInstance<ISelectSellerPresenter>();
                        presenter.Initialize();
                        presenter.Show();
                        presenter.Activate();
                        View.Hide();
                        break;
                    }
            }
        }
    }
}
