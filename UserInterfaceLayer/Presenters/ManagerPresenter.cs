﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ApplicationServicesLayer;
using BusinessLogicLayer.Commands;

using UserInterfaceLayer.Views;
using UserInterfaceLayer.Events;

using DomainModel.Entities;

namespace UserInterfaceLayer.Presenters
{
    public class ManagerPresenter : BasePresenter<IManagerView>, IManagerPresenter
    {
        private List<Seller> _sellers = new List<Seller>();
        //private List<Seller>
        private List<Cheque> _cheques = new List<Cheque>();

        public event EventHandler<ChequeCancelledEventArgs> ChequeCancelled;

        public ManagerPresenter(IApplicationController controller, IManagerView view)
            : base(controller, view)
        {
            View.Closing += OnClosing;
            View.SellerAdding += OnSellerAdding;
            View.SellerFiring += OnSellerFiring;
            View.SellerFired += OnSellerFired;
            View.SellerSelected += OnSellerSelected;
            View.SellerBonusCalculating += OnSellerBonusCalculating;

            View.ChequeViewing += OnChequeViewing;
            View.ChequeCancelPressed += OnChequeCancelPressed;
            View.ChequeCancelling += OnChequeCancelling;
            View.ChequeSelected += OnChequeSelected;

            Controller.GetInstance<IEventAggregator>().Listen(this);

            Controller.GetInstance<IEventAggregator>().SellerAdded += OnSellerAdded;
            Controller.GetInstance<IEventAggregator>().ChequeCancelling += OnChequeCancelling;
        }

        public void Initialize()
        {
            _sellers.Clear();
            _cheques.Clear();

            View.ClearSellerInfo();
            View.ClearCheques();

            // Disable buttons
            this.OnSellerSelected(this, new SellerSelectedEventArgs(-1));

            try
            {
                _sellers.AddRange(Controller.GetInstance<ICommandInvoker>().GetAllSellers().ToArray());
                View.LoadSellers(_sellers.Where(s => !s.FiredState.IsFired).Select(s => s.Name.FullName));

                _cheques.AddRange(Controller.GetInstance<ICommandInvoker>().GetAllCheques().ToArray());
                this.LoadCheques();
            }
            catch (Exception e)
            {
                View.ShowError(e.Message, "Error");
            }
        }

        private void LoadCheques()
        {
            foreach (Cheque cheque in _cheques)
            {
                string chequeId = cheque.ID.Id.ToString();
                Seller seller = _sellers.Find(x => x.ID.Id == cheque.SellerID.Id);
                string chequeSeller = seller != null ? seller.Name.FullName.ToString() : "Unknown seller";
                string chequeDate = cheque.Date.ToString();
                string chequeCancelled = cheque.CancellationState.IsCancelled ? "Отменен" : "Проведен";

                View.AddCheque(chequeId, chequeSeller, chequeDate, chequeCancelled);
            }
        }

        private void OnClosing()
        {
            IAuthPresenter presenter = Controller.GetInstance<IAuthPresenter>();
            presenter.Show();
            presenter.Activate();
            View.Hide();
        }

        private void OnChequeViewing(object sender, ChequeViewingEventArgs e)
        {
            IChequeViewPresenter presenter = Controller.GetInstance<IChequeViewPresenter>();
            Cheque cheque = _cheques[e.ChequeIndex];

            presenter.Initialize(cheque);
            presenter.Show();
            presenter.Activate();
        }

        private void OnChequeCancelPressed()
        {
            View.ShowCancellingDialog();
        }

        private void OnChequeCancelling(object sender, ChequeCancellingEventArgs e)
        {
            Cheque cheque;
            int chequeIndex = -1;

            if (e.Cheque != null)
            {
                cheque = e.Cheque;
            }
            else
            {
                cheque = _cheques[e.ChequeIndex];
            }
            chequeIndex = _cheques.IndexOf(cheque);


            try
            {
                Controller.GetInstance<ICommandInvoker>().CancelCheque(cheque);

                if (ChequeCancelled != null)
                {
                    ChequeCancelled(this, new ChequeCancelledEventArgs(cheque));
                }

                // Update cheque
                cheque.CancellationState = new ChequeCancellationState(true);

                View.EnableChequeCancelling(false);

                View.UpdateChequeCancellationState(chequeIndex, "Отменен");

                View.ShowMessage("Cheque cancelled!", "Success!");
            }
            catch (Exception ex)
            {
                View.ShowError(ex.Message, "Failed to cancel cheque");
            }
            /*else
            {
                View.ShowError("Cheque not selected!", "Error");
            }*/
        }

        private void OnChequeSelected(object sender, ChequeSelectedEventArgs e)
        {
            View.EnableChequeViewing(e.ChequeIndex != -1);
            if (e.ChequeIndex != -1)
            {
                Cheque cheque = _cheques[e.ChequeIndex];
                View.EnableChequeCancelling(!cheque.CancellationState.IsCancelled);
            }
        }


        private void OnSellerAdding()
        {
            IAddSellerPresenter presenter = Controller.GetInstance<IAddSellerPresenter>();
            presenter.Initialize();
            presenter.Show();
            presenter.Activate();
        }

        private void OnSellerAdded(object sender, SellerAddedEventArgs e)
        {
            string sellerName = e.SellerName.Trim();
            string sellerTown = e.SellerTown.Trim();
            string sellerPhone = e.SellerPhone.Trim();

            Name name = new Name(sellerName.Length != 0 ? sellerName : null);
            Town town = new Town(sellerTown.Length != 0 ? sellerTown : null);
            PhoneNumber phone = new PhoneNumber(sellerPhone.Length != 0 ? sellerPhone : null);

            Seller seller = new Seller(null, name, town, phone, new SellerFiredState(false));
            try
            {
                Controller.GetInstance<ICommandInvoker>().AddSeller(seller);
                _sellers.Add(seller);
                Controller.GetInstance<IAddSellerPresenter>().Hide();

                // Update sellers list
                View.LoadSellers(_sellers.Where(s => !s.FiredState.IsFired).Select(s => s.Name.FullName));
                // Disable buttons
                this.OnSellerSelected(this, new SellerSelectedEventArgs(-1));

                View.ShowMessage("Seller added!", "Success");
            }
            catch (Exception ex)
            {
                View.ShowError(ex.Message, "Failed to add seller!");
            }
        }

        private void OnSellerFiring()
        {
            View.ShowFiringDialog();
        }

        private void OnSellerFired(object sender, SellerFiredEventArgs e)
        {
            int sellerIndex = e.SellerIndex;
            if (sellerIndex != -1)
            {
                Seller seller = _sellers.Where(s => !s.FiredState.IsFired).ToArray()[sellerIndex];
                try
                {
                    Controller.GetInstance<ICommandInvoker>().FireSeller(seller);
                    // Update fired state
                    seller.FiredState.IsFired = true;

                    View.LoadSellers(_sellers.Where(s => !s.FiredState.IsFired).Select(s => s.Name.FullName));
                    View.ClearSellerInfo();
                    View.EnableFiring(false);
                    View.EnableBonusCalculating(false);

                    View.ShowMessage("Seller fired!");
                }
                catch (Exception ex)
                {
                    View.ShowError(ex.Message, "Firing failed!");
                }
            }
            else
            {
                View.ShowError("Seller not selected!", "No seller");
            }
        }

        private void OnSellerSelected(object sender, SellerSelectedEventArgs e)
        {
            int sellerIndex = e.SellerIndex;
            if (sellerIndex != -1)
            {
                Seller seller = _sellers.Where(s => !s.FiredState.IsFired).ToArray()[sellerIndex];
                View.ClearSellerInfo();
                View.LoadSellerInfo(seller.Name.FullName, seller.Town.Name, seller.Phone.Number);
                View.EnableFiring(true);
                View.EnableBonusCalculating(true);
            }
            else
            {
                View.EnableFiring(false);
                View.EnableBonusCalculating(false);
                View.ClearSellerInfo();
            }
        }

        private void OnSellerBonusCalculating(object sender, SellerBonusCalculatingEventArgs e)
        {
            int sellerIndex = e.SellerIndex;
            if (sellerIndex != -1)
            {
                try
                {
                    Seller seller = _sellers.Where(s => !s.FiredState.IsFired).ToArray()[sellerIndex];
                    SellerBonus bonus = Controller.GetInstance<ICommandInvoker>().CalculateSellerBonus(seller);
                    View.LoadSellerBonus(bonus.Bonus.ToString());
                }
                catch (Exception ex)
                {
                    View.ShowError(ex.Message, "Failed to calc bonus!");
                }
            }
            else
            {
                View.ShowError("Seller not selected!", "No seller");
            }
        }
    }
}
