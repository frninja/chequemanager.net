﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UserInterfaceLayer.Views;
using DomainModel.Entities;

using ApplicationServicesLayer;

using UserInterfaceLayer.Events;

namespace UserInterfaceLayer.Presenters
{
    

    public class SetCustomCountPresenter : BasePresenter<ISetCustomCountView>, ISetCustomCountPresenter
    {
        public event EventHandler<CustomCountSetEventArgs> CustomCountSet;

        private Custom Custom { get; set; }

        public SetCustomCountPresenter(IApplicationController controller, ISetCustomCountView view) : base(controller, view)
        {
            View.CustomCountSetting += OnCustomCountSetting;
            View.Closing += OnClosing;
            Controller.GetInstance<IEventAggregator>().Listen(this);
        }

        public void Initialize(Custom custom)
        {
            Custom = custom;
            View.ClearCount();
            View.HighlightCount();
        }

        public override void Show()
        {
            base.Show();
            View.HighlightCount();
        }

        private void OnClosing()
        {
            View.Hide();
        }

        private void OnCustomCountSetting(object sender, CustomCountSettingEventArgs e)
        {
            decimal count = e.CustomCount;

            //if (decimal.TryParse(e.CustomCount, out count))
            if (count > 0)
            {
                CustomCount customCount = new CustomCount(count);
                CountedCustom item = new CountedCustom(Custom, customCount);
                CustomCountSet(this, new CustomCountSetEventArgs(Custom, customCount));

                View.ClearCount();
                View.Hide();
            }
            else
            {
                View.ShowError("Custom count can't be zero.", "Error");
            }
        }
    }
}
