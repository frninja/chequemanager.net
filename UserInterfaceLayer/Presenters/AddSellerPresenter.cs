﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UserInterfaceLayer.Events;
using UserInterfaceLayer.Views;
using ApplicationServicesLayer;
using BusinessLogicLayer.Commands;
using DomainModel.Entities;

namespace UserInterfaceLayer.Presenters
{
    public class AddSellerPresenter : BasePresenter<IAddSellerView>, IAddSellerPresenter
    {
        public event EventHandler<SellerAddedEventArgs> SellerAdded;

        public AddSellerPresenter(IApplicationController controller, IAddSellerView view) : base(controller, view)
        {
            View.Closing += OnClosing;
            View.SellerNameValidating += OnSellerNameValidating;
            View.SellerPhoneValidating += OnSellerPhoneValidating;
            View.SellerAdded += OnSellerAdded;
            Controller.GetInstance<IEventAggregator>().Listen(this);
        }

        public void Initialize()
        {
            View.ClearSellerData();
        }

        private void OnClosing()
        {
            View.Hide();
        }

        private void OnSellerNameValidating(object sender, SellerNameValidatingEventArgs e)
        {
            if (e.SellerName != null && e.SellerName != string.Empty)
            {
                View.ValidateSellerName(true);
            }
            else
            {       
                View.ValidateSellerName(false);
                View.ShowError("Seller name can't be empty", "Invalid seller name!");
            }
        }

        private void OnSellerPhoneValidating(object sender, SellerPhoneValidatingEventArgs e)
        {
            if (e.Phone != null)
            {
                foreach (var c in e.Phone)
                {
                    if (!char.IsDigit(c))
                    {
                        View.ValidateSellerPhone(false);
                        View.ShowError("Invalid seller phone! Only digits allowed!", "Invalid seller phone!");
                        break;
                    }
                }
                View.ValidateSellerPhone(true);
            }
        }

        private void OnSellerAdded(object sender, SellerAddedEventArgs e)
        {
            if (SellerAdded != null)
            {
                SellerAdded(this, e);
            }
        }
    }
}
