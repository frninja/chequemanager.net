﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Views
{
    public interface IView
    {
        void Show();
        void Hide();
        void Activate();
        void Close();

        void ShowError(string errorMessage);
        void ShowError(string errorMessage, string errorCaption);
    }
}
