﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;
using UserInterfaceLayer.Events;


namespace UserInterfaceLayer.Views
{

    public interface ISellView : IView
    {
        event Action Closing;
        event Action Exit;

        event Action CustomAdding;
        event Action ChequeCreating;

        event EventHandler<ChequeItemCountValidatingEventArgs> ChequeItemCountValidating;

        event EventHandler<ChequeItemCountChangedEventArgs> ChequeItemCountChanged;

        event EventHandler<ChequeItemRemovedEventArgs> ChequeItemRemoved;

        void AddChequeItem(Custom custom, Warehouse warehouse, CustomCount count);
        void ValidateCell(bool validated);

        void UpdateCountColumn(int rowIndex, decimal count);
        void UpdateSumColumn(int rowIndex, decimal sum);

        void ClearChequeItems();

        void ShowExitDialog();

        void ShowMessage(string message);
        void ShowMessage(string message, string caption);
    }
}
