﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UserInterfaceLayer.Events;

namespace UserInterfaceLayer.Views
{
    public interface IAddSellerView : IView
    {
        event Action Closing;
        event EventHandler<SellerNameValidatingEventArgs> SellerNameValidating;
        event EventHandler<SellerAddedEventArgs> SellerAdded;

        event EventHandler<SellerPhoneValidatingEventArgs> SellerPhoneValidating;

        void ValidateSellerName(bool validated);

        void ValidateSellerPhone(bool validated);
        void ClearSellerData();
    }
}
