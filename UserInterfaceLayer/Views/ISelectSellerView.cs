﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;
using UserInterfaceLayer.Events;

namespace UserInterfaceLayer.Views
{

    public interface ISelectSellerView : IView
    {
        event Action Loaded;
        event Action Closing;
        event EventHandler<LoginEventArgs> Login;

        void LoadSellers(IEnumerable<string> sellers);
        void LoadWarehouses(IEnumerable<string> warehouses);
    }
}
