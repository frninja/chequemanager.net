﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UserInterfaceLayer.Events;
using DomainModel.Entities;

namespace UserInterfaceLayer.Views
{
    public interface IManagerView : IView
    {
        event Action Closing;

        event Action SellerAdding;
        event Action SellerFiring;
        event EventHandler<SellerFiredEventArgs> SellerFired;
        event EventHandler<SellerSelectedEventArgs> SellerSelected;
        event EventHandler<SellerBonusCalculatingEventArgs> SellerBonusCalculating;

        event EventHandler<ChequeViewingEventArgs> ChequeViewing;
        event Action ChequeCancelPressed;
        event EventHandler<ChequeCancellingEventArgs> ChequeCancelling;
        event EventHandler<ChequeSelectedEventArgs> ChequeSelected;

        void ClearCheques();

        void AddCheque(string id, string seller, string date, string state);

        void EnableChequeViewing(bool enabled);
        void EnableChequeCancelling(bool enabled);

        void UpdateChequeCancellationState(int chequeIndex, string state);

        void LoadSellers(IEnumerable<string> sellers);
        void LoadSellerInfo(string name, string town, string phone);

        void LoadSellerBonus(string bonus);

        void RemoveSeller(int sellerIndex);
        void ClearSellerInfo();

        void EnableFiring(bool enabled);
        void EnableBonusCalculating(bool enabled);

        void ShowMessage(string message);
        void ShowMessage(string message, string caption);

        void ShowFiringDialog();

        void ShowCancellingDialog();
    }
}
