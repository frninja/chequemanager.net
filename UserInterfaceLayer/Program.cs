﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using UserInterfaceLayer.Forms;
using UserInterfaceLayer.Presenters;
using UserInterfaceLayer.Views;

using ApplicationServicesLayer;
using BusinessLogicLayer.Commands;

using StructureMap;

namespace UserInterfaceLayer
{
    static class Program
    {
        private static readonly ApplicationContext Context = new ApplicationContext();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            IApplicationController controller = new ApplicationController(Context);

            controller.Configure(x => x.AddRegistry(new InjectRegistry()));

            IAuthPresenter presenter = controller.GetInstance<IAuthPresenter>();
            presenter.Show();
            presenter.Activate();

            Application.Run(Context);
        }
    }
}
