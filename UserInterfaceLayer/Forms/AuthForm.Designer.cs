﻿namespace UserInterfaceLayer.Forms
{
    partial class AuthForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._groupBox = new System.Windows.Forms.GroupBox();
            this._sellerButton = new System.Windows.Forms.Button();
            this._managerButton = new System.Windows.Forms.Button();
            this._groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _groupBox
            // 
            this._groupBox.Controls.Add(this._sellerButton);
            this._groupBox.Controls.Add(this._managerButton);
            this._groupBox.Location = new System.Drawing.Point(12, 12);
            this._groupBox.Name = "_groupBox";
            this._groupBox.Size = new System.Drawing.Size(260, 238);
            this._groupBox.TabIndex = 0;
            this._groupBox.TabStop = false;
            this._groupBox.Text = "Войти как";
            // 
            // _sellerButton
            // 
            this._sellerButton.Location = new System.Drawing.Point(6, 126);
            this._sellerButton.Name = "_sellerButton";
            this._sellerButton.Size = new System.Drawing.Size(248, 101);
            this._sellerButton.TabIndex = 1;
            this._sellerButton.Text = "Продавец";
            this._sellerButton.UseVisualStyleBackColor = true;
            this._sellerButton.Click += new System.EventHandler(this._sellerButton_Click);
            // 
            // _managerButton
            // 
            this._managerButton.Location = new System.Drawing.Point(6, 19);
            this._managerButton.Name = "_managerButton";
            this._managerButton.Size = new System.Drawing.Size(248, 101);
            this._managerButton.TabIndex = 0;
            this._managerButton.Text = "Менеджер";
            this._managerButton.UseVisualStyleBackColor = true;
            this._managerButton.Click += new System.EventHandler(this._sellerButton_Click);
            // 
            // AuthForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this._groupBox);
            this.Name = "AuthForm";
            this.Text = "Вход";
            this._groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox _groupBox;
        private System.Windows.Forms.Button _sellerButton;
        private System.Windows.Forms.Button _managerButton;
    }
}