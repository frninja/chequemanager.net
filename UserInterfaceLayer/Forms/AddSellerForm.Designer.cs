﻿namespace UserInterfaceLayer.Forms
{
    partial class AddSellerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._sellerGroupBox = new System.Windows.Forms.GroupBox();
            this._sellerPhoneTextBox = new System.Windows.Forms.TextBox();
            this._sellerTownTextBox = new System.Windows.Forms.TextBox();
            this._sellerNameTextBox = new System.Windows.Forms.TextBox();
            this._sellerPhoneLabel = new System.Windows.Forms.Label();
            this._sellerTownLabel = new System.Windows.Forms.Label();
            this._sellerNameLabel = new System.Windows.Forms.Label();
            this._addButton = new System.Windows.Forms.Button();
            this._sellerGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _sellerGroupBox
            // 
            this._sellerGroupBox.Controls.Add(this._sellerPhoneTextBox);
            this._sellerGroupBox.Controls.Add(this._sellerTownTextBox);
            this._sellerGroupBox.Controls.Add(this._sellerNameTextBox);
            this._sellerGroupBox.Controls.Add(this._sellerPhoneLabel);
            this._sellerGroupBox.Controls.Add(this._sellerTownLabel);
            this._sellerGroupBox.Controls.Add(this._sellerNameLabel);
            this._sellerGroupBox.Location = new System.Drawing.Point(12, 12);
            this._sellerGroupBox.Name = "_sellerGroupBox";
            this._sellerGroupBox.Size = new System.Drawing.Size(259, 119);
            this._sellerGroupBox.TabIndex = 4;
            this._sellerGroupBox.TabStop = false;
            this._sellerGroupBox.Text = "Информация";
            // 
            // _sellerPhoneTextBox
            // 
            this._sellerPhoneTextBox.Location = new System.Drawing.Point(81, 80);
            this._sellerPhoneTextBox.Name = "_sellerPhoneTextBox";
            this._sellerPhoneTextBox.Size = new System.Drawing.Size(172, 20);
            this._sellerPhoneTextBox.TabIndex = 5;
            this._sellerPhoneTextBox.Validating += new System.ComponentModel.CancelEventHandler(this._sellerPhoneTextBox_Validating);
            // 
            // _sellerTownTextBox
            // 
            this._sellerTownTextBox.Location = new System.Drawing.Point(81, 54);
            this._sellerTownTextBox.Name = "_sellerTownTextBox";
            this._sellerTownTextBox.Size = new System.Drawing.Size(172, 20);
            this._sellerTownTextBox.TabIndex = 4;
            // 
            // _sellerNameTextBox
            // 
            this._sellerNameTextBox.Location = new System.Drawing.Point(81, 25);
            this._sellerNameTextBox.Name = "_sellerNameTextBox";
            this._sellerNameTextBox.Size = new System.Drawing.Size(172, 20);
            this._sellerNameTextBox.TabIndex = 3;
            this._sellerNameTextBox.Validating += new System.ComponentModel.CancelEventHandler(this._sellerNameTextBox_Validating);
            // 
            // _sellerPhoneLabel
            // 
            this._sellerPhoneLabel.Location = new System.Drawing.Point(16, 84);
            this._sellerPhoneLabel.Name = "_sellerPhoneLabel";
            this._sellerPhoneLabel.Size = new System.Drawing.Size(100, 17);
            this._sellerPhoneLabel.TabIndex = 2;
            this._sellerPhoneLabel.Text = "Телефон:";
            // 
            // _sellerTownLabel
            // 
            this._sellerTownLabel.Location = new System.Drawing.Point(16, 54);
            this._sellerTownLabel.Name = "_sellerTownLabel";
            this._sellerTownLabel.Size = new System.Drawing.Size(100, 17);
            this._sellerTownLabel.TabIndex = 1;
            this._sellerTownLabel.Text = "Город:";
            // 
            // _sellerNameLabel
            // 
            this._sellerNameLabel.Location = new System.Drawing.Point(16, 25);
            this._sellerNameLabel.Name = "_sellerNameLabel";
            this._sellerNameLabel.Size = new System.Drawing.Size(100, 17);
            this._sellerNameLabel.TabIndex = 0;
            this._sellerNameLabel.Text = "Имя:";
            // 
            // _addButton
            // 
            this._addButton.Location = new System.Drawing.Point(12, 151);
            this._addButton.Name = "_addButton";
            this._addButton.Size = new System.Drawing.Size(253, 23);
            this._addButton.TabIndex = 5;
            this._addButton.Text = "Добавить";
            this._addButton.UseVisualStyleBackColor = true;
            this._addButton.Click += new System.EventHandler(this._addButton_Click);
            // 
            // AddSellerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 186);
            this.Controls.Add(this._addButton);
            this.Controls.Add(this._sellerGroupBox);
            this.Name = "AddSellerForm";
            this.Text = "Добавить продавца";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddSellerForm_FormClosing);
            this._sellerGroupBox.ResumeLayout(false);
            this._sellerGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox _sellerGroupBox;
        private System.Windows.Forms.TextBox _sellerPhoneTextBox;
        private System.Windows.Forms.TextBox _sellerTownTextBox;
        private System.Windows.Forms.TextBox _sellerNameTextBox;
        private System.Windows.Forms.Label _sellerPhoneLabel;
        private System.Windows.Forms.Label _sellerTownLabel;
        private System.Windows.Forms.Label _sellerNameLabel;
        private System.Windows.Forms.Button _addButton;
    }
}