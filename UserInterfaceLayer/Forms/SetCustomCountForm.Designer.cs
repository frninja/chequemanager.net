﻿namespace UserInterfaceLayer.Forms
{
    partial class SetCustomCountForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._setCountButton = new System.Windows.Forms.Button();
            this._countTextBox = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this._countTextBox)).BeginInit();
            this.SuspendLayout();
            // 
            // _setCountButton
            // 
            this._setCountButton.Location = new System.Drawing.Point(13, 61);
            this._setCountButton.Name = "_setCountButton";
            this._setCountButton.Size = new System.Drawing.Size(259, 23);
            this._setCountButton.TabIndex = 1;
            this._setCountButton.Text = "Добавить товар";
            this._setCountButton.UseVisualStyleBackColor = true;
            this._setCountButton.Click += new System.EventHandler(this._setCountButton_Click);
            // 
            // _countTextBox
            // 
            this._countTextBox.Location = new System.Drawing.Point(13, 22);
            this._countTextBox.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this._countTextBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._countTextBox.Name = "_countTextBox";
            this._countTextBox.Size = new System.Drawing.Size(259, 20);
            this._countTextBox.TabIndex = 3;
            this._countTextBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // SetCustomCountForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 96);
            this.Controls.Add(this._countTextBox);
            this.Controls.Add(this._setCountButton);
            this.Name = "SetCustomCountForm";
            this.Text = "Выбор количества товара";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SetCustomCountForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this._countTextBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _setCountButton;
        private System.Windows.Forms.NumericUpDown _countTextBox;
    }
}