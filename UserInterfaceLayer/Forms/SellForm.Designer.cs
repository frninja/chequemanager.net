﻿namespace UserInterfaceLayer.Forms
{
    partial class SellForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._menuStrip = new System.Windows.Forms.MenuStrip();
            this._dataGridView = new System.Windows.Forms.DataGridView();
            this._addButton = new System.Windows.Forms.Button();
            this.createChequeButton = new System.Windows.Forms.Button();
            this._bindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CustomColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WarehouseColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PriceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CountColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SumColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _menuStrip
            // 
            this._menuStrip.Location = new System.Drawing.Point(0, 0);
            this._menuStrip.Name = "_menuStrip";
            this._menuStrip.Size = new System.Drawing.Size(567, 24);
            this._menuStrip.TabIndex = 0;
            this._menuStrip.Text = "menuStrip1";
            // 
            // _dataGridView
            // 
            this._dataGridView.AllowUserToAddRows = false;
            this._dataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this._dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CustomColumn,
            this.WarehouseColumn,
            this.PriceColumn,
            this.CountColumn,
            this.SumColumn});
            this._dataGridView.Dock = System.Windows.Forms.DockStyle.Top;
            this._dataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this._dataGridView.Location = new System.Drawing.Point(0, 24);
            this._dataGridView.Name = "_dataGridView";
            this._dataGridView.Size = new System.Drawing.Size(567, 209);
            this._dataGridView.TabIndex = 1;
            this._dataGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this._dataGridView_CellValidated);
            this._dataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this._dataGridView_CellValidating);
            this._dataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this._dataGridView_CellValueChanged);
            this._dataGridView.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this._dataGridView_RowsRemoved);
            // 
            // _addButton
            // 
            this._addButton.Location = new System.Drawing.Point(12, 240);
            this._addButton.Name = "_addButton";
            this._addButton.Size = new System.Drawing.Size(288, 23);
            this._addButton.TabIndex = 2;
            this._addButton.Text = "Добавить";
            this._addButton.UseVisualStyleBackColor = true;
            this._addButton.Click += new System.EventHandler(this._addButton_Click);
            // 
            // createChequeButton
            // 
            this.createChequeButton.Location = new System.Drawing.Point(306, 240);
            this.createChequeButton.Name = "createChequeButton";
            this.createChequeButton.Size = new System.Drawing.Size(249, 23);
            this.createChequeButton.TabIndex = 3;
            this.createChequeButton.Text = "Выбить чек";
            this.createChequeButton.UseVisualStyleBackColor = true;
            this.createChequeButton.Click += new System.EventHandler(this.createChequeButton_Click);
            // 
            // CustomColumn
            // 
            this.CustomColumn.HeaderText = "Товар";
            this.CustomColumn.Name = "CustomColumn";
            this.CustomColumn.ReadOnly = true;
            this.CustomColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // WarehouseColumn
            // 
            this.WarehouseColumn.HeaderText = "Склад";
            this.WarehouseColumn.Name = "WarehouseColumn";
            this.WarehouseColumn.ReadOnly = true;
            this.WarehouseColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PriceColumn
            // 
            this.PriceColumn.HeaderText = "Цена";
            this.PriceColumn.Name = "PriceColumn";
            this.PriceColumn.ReadOnly = true;
            this.PriceColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CountColumn
            // 
            this.CountColumn.HeaderText = "Количество";
            this.CountColumn.Name = "CountColumn";
            this.CountColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // SumColumn
            // 
            this.SumColumn.HeaderText = "Сумма";
            this.SumColumn.Name = "SumColumn";
            this.SumColumn.ReadOnly = true;
            this.SumColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // SellForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 275);
            this.Controls.Add(this.createChequeButton);
            this.Controls.Add(this._addButton);
            this.Controls.Add(this._dataGridView);
            this.Controls.Add(this._menuStrip);
            this.MainMenuStrip = this._menuStrip;
            this.Name = "SellForm";
            this.Text = "Seller";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SellForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip _menuStrip;
        private System.Windows.Forms.DataGridView _dataGridView;
        private System.Windows.Forms.Button _addButton;
        private System.Windows.Forms.Button createChequeButton;
        private System.Windows.Forms.BindingSource _bindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn WarehouseColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn PriceColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CountColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn SumColumn;

    }
}