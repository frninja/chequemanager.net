﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using UserInterfaceLayer.Events;
using UserInterfaceLayer.Views;

namespace UserInterfaceLayer.Forms
{
    public partial class AddSellerForm : Form, IAddSellerView
    {
        private readonly ApplicationContext _context;

        private bool _sellerNameValidated;
        private bool _sellerPhoneValidated;

        public new event Action Closing;
        public event EventHandler<SellerNameValidatingEventArgs> SellerNameValidating;
        public event EventHandler<SellerAddedEventArgs> SellerAdded;

        public event EventHandler<SellerPhoneValidatingEventArgs> SellerPhoneValidating;
        

        public AddSellerForm(ApplicationContext context)
        {
            _context = context;
            InitializeComponent();
        }

        public new void Show()
        {
            _context.MainForm = this;
            base.Show();
        }

        public new void Close()
        {
            base.Close();
        }

        public void ShowError(string errorMessage)
        {
            MessageBox.Show(errorMessage);
        }

        public void ShowError(string errorMessage, string errorCaption)
        {
            MessageBox.Show(errorMessage, errorCaption);
        }

        public void ValidateSellerName(bool validated)
        {
            _sellerNameValidated = validated;
        }

        public void ValidateSellerPhone(bool validated)
        {
            _sellerPhoneValidated = validated;
        }

        public void ClearSellerData()
        {
            _sellerNameTextBox.Text = string.Empty;
            _sellerTownTextBox.Text = string.Empty;
            _sellerPhoneTextBox.Text = string.Empty;
        }

        private void AddSellerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing && Closing != null)
            {
                Closing();
                e.Cancel = true;
            }
        }

        private void _addButton_Click(object sender, EventArgs e)
        {
            if (SellerAdded != null)
            {
                string sellerName = _sellerNameTextBox.Text;
                string sellerTown = _sellerTownTextBox.Text;
                string sellerPhone = _sellerPhoneTextBox.Text;
                SellerAdded(this, new SellerAddedEventArgs(sellerName, sellerTown, sellerPhone));
            }
        }

        private void _sellerNameTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (SellerNameValidating != null)
            {
                string sellerName = _sellerNameTextBox.Text;
                SellerNameValidating(this, new SellerNameValidatingEventArgs(sellerName));
                if (!_sellerNameValidated)
                {
                    e.Cancel = true;
                }
            }
        }

        private void _sellerPhoneTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (SellerPhoneValidating != null)
            {
                string sellerPhone = _sellerPhoneTextBox.Text;
                SellerPhoneValidating(this, new SellerPhoneValidatingEventArgs(sellerPhone));
                if (!_sellerPhoneValidated)
                {
                    e.Cancel = true;
                }
            }
        }


    }
}
