﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using UserInterfaceLayer.Views;
using UserInterfaceLayer.Events;


namespace UserInterfaceLayer.Forms
{
    public partial class ManagerForm : Form, IManagerView
    {
        private readonly ApplicationContext _context;

        public new event Action Closing;

        public event Action SellerAdding;
        public event Action SellerFiring;
        public event EventHandler<SellerFiredEventArgs> SellerFired;
        public event EventHandler<SellerSelectedEventArgs> SellerSelected;
        public event EventHandler<SellerBonusCalculatingEventArgs> SellerBonusCalculating;

        public event EventHandler<ChequeViewingEventArgs> ChequeViewing;
        public event Action ChequeCancelPressed;
        public event EventHandler<ChequeCancellingEventArgs> ChequeCancelling;
        public event EventHandler<ChequeSelectedEventArgs> ChequeSelected;

        public ManagerForm(ApplicationContext context)
        {
            _context = context;
            InitializeComponent();
        }

        public new void Show()
        {
            _context.MainForm = this;
            base.Show();
        }

        public new void Close()
        {
            base.Close();
        }

        public void ShowError(string errorMessage)
        {
            MessageBox.Show(errorMessage);
        }

        public void ShowError(string errorMessage, string errorCaption)
        {
            MessageBox.Show(errorMessage, errorCaption);
        }

        public void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }

        public void ShowMessage(string message, string caption)
        {
            MessageBox.Show(message, caption);
        }

        public void ShowFiringDialog()
        {
            if (MessageBox.Show("Fire this seller?", "Firing", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                int sellerIndex = _sellersList.SelectedIndex;

                if (SellerFired != null)
                {
                    SellerFired(this, new SellerFiredEventArgs(sellerIndex));
                }
            }
        }

        

        #region Cheques
        public void ClearCheques()
        {
            _chequesDataGridView.Rows.Clear();
        }

        public void AddCheque(string id, string seller, string date, string state)
        {
            _chequesDataGridView.Rows.Add(id, seller, date, state);
        }

        public void EnableChequeViewing(bool enabled)
        {
            _viewChequeButton.Enabled = enabled;
        }

        public void EnableChequeCancelling(bool enabled)
        {
            _cancelChequeButton.Enabled = enabled;
        }

        public void UpdateChequeCancellationState(int chequeIndex, string state)
        {
            _chequesDataGridView.Rows[chequeIndex].Cells["ChequeStatusColumn"].Value = state;
        }

        public void ShowCancellingDialog()
        {
            if (MessageBox.Show("Sure?", "Cancel cheque.", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                if (ChequeCancelling != null)
                {
                    //int chequeIndex = _chequesDataGridView.SelectedRows[0].Index;
                    int chequeIndex = _chequesDataGridView.SelectedCells[0].RowIndex;
                    ChequeCancelling(this, new ChequeCancellingEventArgs(chequeIndex));
                }
            }
        }

        #endregion

        public void LoadSellers(IEnumerable<string> sellers)
        {
            _sellersList.Items.Clear();
            _sellersList.Items.AddRange(sellers.ToArray());
        }

        public void LoadSellerInfo(string name, string town, string phone)
        {
            _sellerNameTextBox.Text = name;
            _sellerTownTextBox.Text = town;
            _sellerPhoneTextBox.Text = phone;
        }

        public void LoadSellerBonus(string bonus)
        {
            _sellerBonusTextBox.Text = bonus;
        }

        public void ClearSellerInfo()
        {
            _sellerNameTextBox.Text = string.Empty;
            _sellerTownTextBox.Text = string.Empty;
            _sellerPhoneTextBox.Text = string.Empty;
            _sellerBonusTextBox.Text = string.Empty;
        }

        public void EnableFiring(bool enabled)
        {
            _fireButton.Enabled = enabled;
        }

        public void EnableBonusCalculating(bool enabled)
        {
            _calcSellerBonusButton.Enabled = enabled;
        }

        public void RemoveSeller(int sellerIndex)
        {
            _sellersList.Items.RemoveAt(sellerIndex);
        }

        private void _sellersList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int sellerIndex = _sellersList.SelectedIndex;

            if (SellerSelected != null)
            {
                SellerSelected(this, new SellerSelectedEventArgs(sellerIndex));
            }
        }

        private void _addButton_Click(object sender, EventArgs e)
        {
            if (SellerAdding != null)
            {
                SellerAdding();
            }
        }

        private void _fireButton_Click(object sender, EventArgs e)
        {
            if (SellerFiring != null)
            {
                SellerFiring();
            }
        }

        private void ManagerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Closing != null)
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    Closing();
                    e.Cancel = true;
                }
            }
        }

        private void _calcSellerBonusButton_Click(object sender, EventArgs e)
        {
            if (SellerBonusCalculating != null)
            {
                int sellerIndex = _sellersList.SelectedIndex;
                SellerBonusCalculating(this, new SellerBonusCalculatingEventArgs(sellerIndex));
            }
        }

        private void _viewChequeButton_Click(object sender, EventArgs e)
        {
            if (ChequeViewing != null)
            {
                int chequeIndex = _chequesDataGridView.SelectedCells[0].RowIndex;
                //int chequeIndex = _chequesDataGridView.SelectedRows[0].Index;
                ChequeViewing(this, new ChequeViewingEventArgs(chequeIndex));
            }
        }

        private void _cancelChequeButton_Click(object sender, EventArgs e)
        {
            if (ChequeCancelPressed != null)
            {
                ChequeCancelPressed();
            }
        }

        private void _chequesDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (ChequeSelected != null)
            {
                if (_chequesDataGridView.SelectedCells.Count > 0)
                {
                    int chequeIndex = _chequesDataGridView.SelectedCells[0].RowIndex;
                    ChequeSelected(this, new ChequeSelectedEventArgs(chequeIndex));
                }
                else
                {
                    ChequeSelected(this, new ChequeSelectedEventArgs(-1));
                }
            }
        }

    }
}
