﻿namespace UserInterfaceLayer.Forms
{
    partial class ManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._tabControl = new System.Windows.Forms.TabControl();
            this._sellersTab = new System.Windows.Forms.TabPage();
            this._chequesTab = new System.Windows.Forms.TabPage();
            this._sellersList = new System.Windows.Forms.ListBox();
            this._addButton = new System.Windows.Forms.Button();
            this._fireButton = new System.Windows.Forms.Button();
            this._sellerGroupBox = new System.Windows.Forms.GroupBox();
            this._sellerNameLabel = new System.Windows.Forms.Label();
            this._sellerTownLabel = new System.Windows.Forms.Label();
            this._sellerPhoneLabel = new System.Windows.Forms.Label();
            this._sellerNameTextBox = new System.Windows.Forms.TextBox();
            this._sellerTownTextBox = new System.Windows.Forms.TextBox();
            this._sellerPhoneTextBox = new System.Windows.Forms.TextBox();
            this._sellerBonusLabel = new System.Windows.Forms.Label();
            this._sellerBonusTextBox = new System.Windows.Forms.TextBox();
            this._calcSellerBonusButton = new System.Windows.Forms.Button();
            this._chequesDataGridView = new System.Windows.Forms.DataGridView();
            this._viewChequeButton = new System.Windows.Forms.Button();
            this._cancelChequeButton = new System.Windows.Forms.Button();
            this.ChequeIdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SellerColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChequeStatusColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tabControl.SuspendLayout();
            this._sellersTab.SuspendLayout();
            this._chequesTab.SuspendLayout();
            this._sellerGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._chequesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // _tabControl
            // 
            this._tabControl.Controls.Add(this._sellersTab);
            this._tabControl.Controls.Add(this._chequesTab);
            this._tabControl.Location = new System.Drawing.Point(12, 12);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(523, 238);
            this._tabControl.TabIndex = 0;
            // 
            // _sellersTab
            // 
            this._sellersTab.Controls.Add(this._sellerGroupBox);
            this._sellersTab.Controls.Add(this._fireButton);
            this._sellersTab.Controls.Add(this._addButton);
            this._sellersTab.Controls.Add(this._sellersList);
            this._sellersTab.Location = new System.Drawing.Point(4, 22);
            this._sellersTab.Name = "_sellersTab";
            this._sellersTab.Padding = new System.Windows.Forms.Padding(3);
            this._sellersTab.Size = new System.Drawing.Size(515, 212);
            this._sellersTab.TabIndex = 0;
            this._sellersTab.Text = "Продавцы";
            this._sellersTab.UseVisualStyleBackColor = true;
            // 
            // _chequesTab
            // 
            this._chequesTab.Controls.Add(this._cancelChequeButton);
            this._chequesTab.Controls.Add(this._viewChequeButton);
            this._chequesTab.Controls.Add(this._chequesDataGridView);
            this._chequesTab.Location = new System.Drawing.Point(4, 22);
            this._chequesTab.Name = "_chequesTab";
            this._chequesTab.Padding = new System.Windows.Forms.Padding(3);
            this._chequesTab.Size = new System.Drawing.Size(515, 212);
            this._chequesTab.TabIndex = 1;
            this._chequesTab.Text = "Чеки";
            this._chequesTab.UseVisualStyleBackColor = true;
            // 
            // _sellersList
            // 
            this._sellersList.FormattingEnabled = true;
            this._sellersList.Location = new System.Drawing.Point(6, 16);
            this._sellersList.Name = "_sellersList";
            this._sellersList.Size = new System.Drawing.Size(215, 160);
            this._sellersList.TabIndex = 0;
            this._sellersList.SelectedIndexChanged += new System.EventHandler(this._sellersList_SelectedIndexChanged);
            // 
            // _addButton
            // 
            this._addButton.Location = new System.Drawing.Point(6, 183);
            this._addButton.Name = "_addButton";
            this._addButton.Size = new System.Drawing.Size(117, 23);
            this._addButton.TabIndex = 1;
            this._addButton.Text = "Добавить продавца";
            this._addButton.UseVisualStyleBackColor = true;
            this._addButton.Click += new System.EventHandler(this._addButton_Click);
            // 
            // _fireButton
            // 
            this._fireButton.Enabled = false;
            this._fireButton.Location = new System.Drawing.Point(130, 183);
            this._fireButton.Name = "_fireButton";
            this._fireButton.Size = new System.Drawing.Size(91, 23);
            this._fireButton.TabIndex = 2;
            this._fireButton.Text = "Уволить";
            this._fireButton.UseVisualStyleBackColor = true;
            this._fireButton.Click += new System.EventHandler(this._fireButton_Click);
            // 
            // _sellerGroupBox
            // 
            this._sellerGroupBox.Controls.Add(this._calcSellerBonusButton);
            this._sellerGroupBox.Controls.Add(this._sellerBonusTextBox);
            this._sellerGroupBox.Controls.Add(this._sellerBonusLabel);
            this._sellerGroupBox.Controls.Add(this._sellerPhoneTextBox);
            this._sellerGroupBox.Controls.Add(this._sellerTownTextBox);
            this._sellerGroupBox.Controls.Add(this._sellerNameTextBox);
            this._sellerGroupBox.Controls.Add(this._sellerPhoneLabel);
            this._sellerGroupBox.Controls.Add(this._sellerTownLabel);
            this._sellerGroupBox.Controls.Add(this._sellerNameLabel);
            this._sellerGroupBox.Location = new System.Drawing.Point(250, 16);
            this._sellerGroupBox.Name = "_sellerGroupBox";
            this._sellerGroupBox.Size = new System.Drawing.Size(259, 190);
            this._sellerGroupBox.TabIndex = 3;
            this._sellerGroupBox.TabStop = false;
            this._sellerGroupBox.Text = "Информация";
            // 
            // _sellerNameLabel
            // 
            this._sellerNameLabel.Location = new System.Drawing.Point(16, 25);
            this._sellerNameLabel.Name = "_sellerNameLabel";
            this._sellerNameLabel.Size = new System.Drawing.Size(100, 17);
            this._sellerNameLabel.TabIndex = 0;
            this._sellerNameLabel.Text = "Имя:";
            // 
            // _sellerTownLabel
            // 
            this._sellerTownLabel.Location = new System.Drawing.Point(16, 54);
            this._sellerTownLabel.Name = "_sellerTownLabel";
            this._sellerTownLabel.Size = new System.Drawing.Size(100, 17);
            this._sellerTownLabel.TabIndex = 1;
            this._sellerTownLabel.Text = "Город:";
            // 
            // _sellerPhoneLabel
            // 
            this._sellerPhoneLabel.Location = new System.Drawing.Point(16, 84);
            this._sellerPhoneLabel.Name = "_sellerPhoneLabel";
            this._sellerPhoneLabel.Size = new System.Drawing.Size(100, 17);
            this._sellerPhoneLabel.TabIndex = 2;
            this._sellerPhoneLabel.Text = "Телефон:";
            // 
            // _sellerNameTextBox
            // 
            this._sellerNameTextBox.Location = new System.Drawing.Point(81, 25);
            this._sellerNameTextBox.Name = "_sellerNameTextBox";
            this._sellerNameTextBox.ReadOnly = true;
            this._sellerNameTextBox.Size = new System.Drawing.Size(172, 20);
            this._sellerNameTextBox.TabIndex = 3;
            // 
            // _sellerTownTextBox
            // 
            this._sellerTownTextBox.Location = new System.Drawing.Point(81, 54);
            this._sellerTownTextBox.Name = "_sellerTownTextBox";
            this._sellerTownTextBox.ReadOnly = true;
            this._sellerTownTextBox.Size = new System.Drawing.Size(172, 20);
            this._sellerTownTextBox.TabIndex = 4;
            // 
            // _sellerPhoneTextBox
            // 
            this._sellerPhoneTextBox.Location = new System.Drawing.Point(81, 80);
            this._sellerPhoneTextBox.Name = "_sellerPhoneTextBox";
            this._sellerPhoneTextBox.ReadOnly = true;
            this._sellerPhoneTextBox.Size = new System.Drawing.Size(172, 20);
            this._sellerPhoneTextBox.TabIndex = 5;
            // 
            // _sellerBonusLabel
            // 
            this._sellerBonusLabel.Location = new System.Drawing.Point(16, 109);
            this._sellerBonusLabel.Name = "_sellerBonusLabel";
            this._sellerBonusLabel.Size = new System.Drawing.Size(100, 17);
            this._sellerBonusLabel.TabIndex = 6;
            this._sellerBonusLabel.Text = "Бонус:";
            // 
            // _sellerBonusTextBox
            // 
            this._sellerBonusTextBox.Location = new System.Drawing.Point(81, 106);
            this._sellerBonusTextBox.Name = "_sellerBonusTextBox";
            this._sellerBonusTextBox.ReadOnly = true;
            this._sellerBonusTextBox.Size = new System.Drawing.Size(172, 20);
            this._sellerBonusTextBox.TabIndex = 7;
            // 
            // _calcSellerBonusButton
            // 
            this._calcSellerBonusButton.Enabled = false;
            this._calcSellerBonusButton.Location = new System.Drawing.Point(19, 137);
            this._calcSellerBonusButton.Name = "_calcSellerBonusButton";
            this._calcSellerBonusButton.Size = new System.Drawing.Size(234, 23);
            this._calcSellerBonusButton.TabIndex = 8;
            this._calcSellerBonusButton.Text = "Посчитать бонус";
            this._calcSellerBonusButton.UseVisualStyleBackColor = true;
            this._calcSellerBonusButton.Click += new System.EventHandler(this._calcSellerBonusButton_Click);
            // 
            // _chequesDataGridView
            // 
            this._chequesDataGridView.AllowUserToAddRows = false;
            this._chequesDataGridView.AllowUserToDeleteRows = false;
            this._chequesDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this._chequesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._chequesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ChequeIdColumn,
            this.SellerColumn,
            this.DateColumn,
            this.ChequeStatusColumn});
            this._chequesDataGridView.Location = new System.Drawing.Point(6, 6);
            this._chequesDataGridView.MultiSelect = false;
            this._chequesDataGridView.Name = "_chequesDataGridView";
            this._chequesDataGridView.Size = new System.Drawing.Size(444, 170);
            this._chequesDataGridView.TabIndex = 0;
            this._chequesDataGridView.SelectionChanged += new System.EventHandler(this._chequesDataGridView_SelectionChanged);
            // 
            // _viewChequeButton
            // 
            this._viewChequeButton.Enabled = false;
            this._viewChequeButton.Location = new System.Drawing.Point(6, 183);
            this._viewChequeButton.Name = "_viewChequeButton";
            this._viewChequeButton.Size = new System.Drawing.Size(230, 23);
            this._viewChequeButton.TabIndex = 1;
            this._viewChequeButton.Text = "Просмотр";
            this._viewChequeButton.UseVisualStyleBackColor = true;
            this._viewChequeButton.Click += new System.EventHandler(this._viewChequeButton_Click);
            // 
            // _cancelChequeButton
            // 
            this._cancelChequeButton.Enabled = false;
            this._cancelChequeButton.Location = new System.Drawing.Point(242, 183);
            this._cancelChequeButton.Name = "_cancelChequeButton";
            this._cancelChequeButton.Size = new System.Drawing.Size(208, 23);
            this._cancelChequeButton.TabIndex = 2;
            this._cancelChequeButton.Text = "Отменить";
            this._cancelChequeButton.UseVisualStyleBackColor = true;
            this._cancelChequeButton.Click += new System.EventHandler(this._cancelChequeButton_Click);
            // 
            // ChequeIdColumn
            // 
            this.ChequeIdColumn.HeaderText = "Номер чека";
            this.ChequeIdColumn.Name = "ChequeIdColumn";
            this.ChequeIdColumn.ReadOnly = true;
            this.ChequeIdColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // SellerColumn
            // 
            this.SellerColumn.HeaderText = "Продавец";
            this.SellerColumn.Name = "SellerColumn";
            this.SellerColumn.ReadOnly = true;
            this.SellerColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // DateColumn
            // 
            this.DateColumn.HeaderText = "Дата";
            this.DateColumn.Name = "DateColumn";
            this.DateColumn.ReadOnly = true;
            this.DateColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ChequeStatusColumn
            // 
            this.ChequeStatusColumn.HeaderText = "Статус";
            this.ChequeStatusColumn.Name = "ChequeStatusColumn";
            this.ChequeStatusColumn.ReadOnly = true;
            this.ChequeStatusColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 262);
            this.Controls.Add(this._tabControl);
            this.Name = "ManagerForm";
            this.Text = "Менеджер";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ManagerForm_FormClosing);
            this._tabControl.ResumeLayout(false);
            this._sellersTab.ResumeLayout(false);
            this._chequesTab.ResumeLayout(false);
            this._sellerGroupBox.ResumeLayout(false);
            this._sellerGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._chequesDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl _tabControl;
        private System.Windows.Forms.TabPage _sellersTab;
        private System.Windows.Forms.GroupBox _sellerGroupBox;
        private System.Windows.Forms.TextBox _sellerPhoneTextBox;
        private System.Windows.Forms.TextBox _sellerTownTextBox;
        private System.Windows.Forms.TextBox _sellerNameTextBox;
        private System.Windows.Forms.Label _sellerPhoneLabel;
        private System.Windows.Forms.Label _sellerTownLabel;
        private System.Windows.Forms.Label _sellerNameLabel;
        private System.Windows.Forms.Button _fireButton;
        private System.Windows.Forms.Button _addButton;
        private System.Windows.Forms.ListBox _sellersList;
        private System.Windows.Forms.TabPage _chequesTab;
        private System.Windows.Forms.Button _calcSellerBonusButton;
        private System.Windows.Forms.TextBox _sellerBonusTextBox;
        private System.Windows.Forms.Label _sellerBonusLabel;
        private System.Windows.Forms.DataGridView _chequesDataGridView;
        private System.Windows.Forms.Button _cancelChequeButton;
        private System.Windows.Forms.Button _viewChequeButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChequeIdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn SellerColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChequeStatusColumn;
    }
}