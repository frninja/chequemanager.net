﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;
using UserInterfaceLayer.Presenters;

using UserInterfaceLayer.Events;

namespace UserInterfaceLayer
{
    public class EventAggregator : IEventAggregator
    {
        public event EventHandler<CustomSelectedEventArgs> CustomSelected;
        public event EventHandler<SellerAddedEventArgs> SellerAdded;
        public event EventHandler<ChequeCancellingEventArgs> ChequeCancelling;
        public event EventHandler<ChequeCancelledEventArgs> ChequeCancelled;

        public void Listen(ISetCustomCountPresenter subject)
        {
            subject.CustomCountSet += OnCustomCountSet;
        }

        public void Listen(IAddSellerPresenter subject)
        {
            subject.SellerAdded += OnSellerAdded;
        }

        public void Listen(IChequeViewPresenter subject)
        {
            subject.ChequeCancelling += OnChequeCancelling;
        }

        public void Listen(IManagerPresenter subject)
        {
            subject.ChequeCancelled += OnChequeCancelled;
        }

        private void OnCustomCountSet(object sender, CustomCountSetEventArgs e)
        {
            CustomSelected(sender, new CustomSelectedEventArgs(e.Custom, e.Count));
        }

        private void OnSellerAdded(object sender, SellerAddedEventArgs e)
        {
            if (SellerAdded != null)
            {
                SellerAdded(this, e);
            }
        }

        private void OnChequeCancelling(object sender, ChequeCancellingEventArgs e)
        {
            if (ChequeCancelling != null)
            {
                ChequeCancelling(this, e);
            }
        }

        private void OnChequeCancelled(object sender, ChequeCancelledEventArgs e)
        {
            if (ChequeCancelled != null)
            {
                ChequeCancelled(this, e);
            }
        }
    }
}
