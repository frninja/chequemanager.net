﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;

namespace UserInterfaceLayer.Events
{
    public class CustomSelectedEventArgs : EventArgs
    {
        public Custom Custom { get; private set; }

        public CustomCount Count { get; private set; }

        public CustomSelectedEventArgs(Custom custom, CustomCount count)
        {
            Custom = custom;
            Count = count;
        }
    }
}
