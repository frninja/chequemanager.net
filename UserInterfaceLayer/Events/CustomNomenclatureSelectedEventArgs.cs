﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Events
{
    public class CustomNomenclatureSelectedEventArgs : EventArgs
    {
        public int CustomIndex { get; set; }

        public CustomNomenclatureSelectedEventArgs(int customIndex)
        {
            CustomIndex = customIndex;
        }
    }
}
