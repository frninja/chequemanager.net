﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Events
{
    public class ChequeItemCountValidatingEventArgs : EventArgs
    {
        public object FormattedValue { get; private set; }

        public ChequeItemCountValidatingEventArgs(object value)
        {
            FormattedValue = value;
        }
    }
}
