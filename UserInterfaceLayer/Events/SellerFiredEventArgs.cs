﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Events
{
    public class SellerFiredEventArgs : EventArgs
    {
        public int SellerIndex { get; private set; }
        
        public SellerFiredEventArgs(int sellerIndex)
        {
            SellerIndex = sellerIndex;
        }
    }
}
