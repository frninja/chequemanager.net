﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Events
{
    public class ChequeItemRemovedEventArgs : EventArgs
    {
        public int RowCount { get; private set; }
        public int RowIndex { get; private set; }

        public ChequeItemRemovedEventArgs(int rowCount, int rowIndex)
        {
            RowCount = rowCount;
            RowIndex = rowIndex;
        }
    }
}
