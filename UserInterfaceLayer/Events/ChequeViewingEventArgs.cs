﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Events
{
    public class ChequeViewingEventArgs : EventArgs 
    {
        public int ChequeIndex { get; private set; }

        public ChequeViewingEventArgs(int chequeIndex)
        {
            ChequeIndex = chequeIndex;
        }
    }
}
