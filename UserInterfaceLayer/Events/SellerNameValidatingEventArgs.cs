﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Events
{
    public class SellerNameValidatingEventArgs : EventArgs
    {
        public string SellerName { get; private set; }

        public SellerNameValidatingEventArgs(string name)
        {
            SellerName = name;
        }
    }
}
