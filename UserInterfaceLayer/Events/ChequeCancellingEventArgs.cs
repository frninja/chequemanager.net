﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;

namespace UserInterfaceLayer.Events
{
    public class ChequeCancellingEventArgs : EventArgs
    {
        public int ChequeIndex { get; private set; }

        public Cheque Cheque { get; private set; }

        public ChequeCancellingEventArgs(int chequeIndex)
        {
            ChequeIndex = chequeIndex;
        }

        public ChequeCancellingEventArgs(Cheque cheque)
        {
            Cheque = cheque;
        }
    }
}
