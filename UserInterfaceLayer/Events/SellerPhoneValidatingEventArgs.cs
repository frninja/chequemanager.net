﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UserInterfaceLayer.Events
{
    public class SellerPhoneValidatingEventArgs : EventArgs
    {
        public string Phone { get; private set; }
        public SellerPhoneValidatingEventArgs(string phone)
        {
            Phone = phone;
        }
    }
}
