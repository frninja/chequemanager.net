﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Events
{
    public class ChequeItemCountChangedEventArgs : EventArgs
    {
        public int ItemIndex { get; private set; }

        public string CustomCount { get; private set; }

        public ChequeItemCountChangedEventArgs(int itemIndex, string customCount)
        {
            ItemIndex = itemIndex;
            CustomCount = customCount;
        }
    }
}
