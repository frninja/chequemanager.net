﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Events
{
    public class LoggingInEventArgs : EventArgs
    {
        public AuthType AuthType { get; private set; }

        public LoggingInEventArgs(AuthType type)
        {
            AuthType = type;
        }
    }

    public enum AuthType { SELLER, MANAGER };
}
