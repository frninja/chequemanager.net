﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer.Events
{
    public class CustomCountSettingEventArgs : EventArgs
    {
        public decimal CustomCount { get; private set; }

        public CustomCountSettingEventArgs(decimal count)
        {
            CustomCount = count;
        }
    }
}
