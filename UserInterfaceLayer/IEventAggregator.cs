﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UserInterfaceLayer.Presenters;
using UserInterfaceLayer.Events;
using DomainModel.Entities;

namespace UserInterfaceLayer
{
    public interface IEventAggregator
    {
        event EventHandler<CustomSelectedEventArgs> CustomSelected;
        event EventHandler<SellerAddedEventArgs> SellerAdded;
        event EventHandler<ChequeCancellingEventArgs> ChequeCancelling;
        event EventHandler<ChequeCancelledEventArgs> ChequeCancelled;

        void Listen(ISetCustomCountPresenter subject);
        void Listen(IAddSellerPresenter subject);
        void Listen(IChequeViewPresenter subject);
        void Listen(IManagerPresenter subject);
    }
}
