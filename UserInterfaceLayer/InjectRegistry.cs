﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

using StructureMap.Configuration.DSL;

using UserInterfaceLayer.Views;
using UserInterfaceLayer.Forms;
using UserInterfaceLayer.Presenters;

using DataAccessLayer.Gateway;

using ApplicationServicesLayer;
using BusinessLogicLayer.Commands;

namespace UserInterfaceLayer
{
    public class InjectRegistry : Registry
    {
        public InjectRegistry()
        {
            For<ICommandInvoker>().Singleton().Use<CommandInvoker>();

            For<IEventAggregator>().Singleton().Use<EventAggregator>();

            For<IDatabaseGateway>().Singleton().Use<DatabaseGateway>();

            For<ICustomGateway>().Singleton().Use<CustomGateway>().Ctor<IDatabaseGateway>();
            For<IChequeGateway>().Singleton().Use<ChequeGateway>().Ctor<IDatabaseGateway>();
            For<ISellerGateway>().Singleton().Use<SellerGateway>().Ctor<IDatabaseGateway>();
            For<IWarehouseGateway>().Singleton().Use<WarehouseGateway>().Ctor<IDatabaseGateway>();
            For<ICustomWarehouseGateway>().Singleton().Use<CustomWarehouseGateway>().Ctor<IDatabaseGateway>();

            For<IAuthView>().Use<AuthForm>().Ctor<ApplicationContext>();
            For<ISelectSellerView>().Use<SelectSellerForm>().Ctor<ApplicationContext>();
            For<ISellView>().Use<SellForm>().Ctor<ApplicationContext>();
            For<ISelectCustomNomenclatureView>().Use<SelectCustomNomenclatureForm>().Ctor<ApplicationContext>();
            For<ISetCustomCountView>().Use<SetCustomCountForm>().Ctor<ApplicationContext>();
            For<IManagerView>().Use<ManagerForm>();
            For<IAddSellerView>().Use<AddSellerForm>();
            For<IChequeView>().Use<ChequeViewForm>();
            

            For<IAuthPresenter>().Singleton().Use<AuthPresenter>().Ctor<IApplicationController>("controller");
            For<IAuthPresenter>().Singleton().Use<AuthPresenter>().Ctor<IAuthView>("view");

            For<ISelectSellerPresenter>().Singleton().Use<SelectSellerPresenter>().Ctor<IApplicationController>("controller");
            For<ISelectSellerPresenter>().Singleton().Use<SelectSellerPresenter>().Ctor<ISelectSellerView>("view");

            For<ISellPresenter>().Singleton().Use<SellPresenter>().Ctor<IApplicationController>("controller");
            For<ISellPresenter>().Singleton().Use<SellPresenter>().Ctor<ISellView>("view");

            For<ISelectCustomNomenclaturePresenter>().Singleton().Use<SelectCustomNomenclaturePresenter>().Ctor<IApplicationController>("controller");
            For<ISelectCustomNomenclaturePresenter>().Singleton().Use<SelectCustomNomenclaturePresenter>().Ctor<ISelectCustomNomenclatureView>("view");

            For<ISetCustomCountPresenter>().Singleton().Use<SetCustomCountPresenter>().Ctor<IApplicationController>("controller");
            For<ISetCustomCountPresenter>().Singleton().Use<SetCustomCountPresenter>().Ctor<ISelectCustomNomenclatureView>("view");

            For<IManagerPresenter>().Singleton().Use<ManagerPresenter>();
            For<IAddSellerPresenter>().Singleton().Use<AddSellerPresenter>();
            For<IChequeViewPresenter>().Singleton().Use<ChequeViewPresenter>();
        }
    }
}
