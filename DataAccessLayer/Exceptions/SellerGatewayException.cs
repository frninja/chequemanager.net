﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class SellerGatewayException : Exception
    {
        public SellerGatewayException(string message)
            : base(message)
        {
        }

        public SellerGatewayException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
