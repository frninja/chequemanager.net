﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class CustomWarehouseGatewayException : Exception
    {
        public CustomWarehouseGatewayException(string message)
            : base(message)
        {
        }

        public CustomWarehouseGatewayException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
