﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DomainModel.Entities;

namespace DataAccessLayer.Gateway
{
    public interface ICustomGateway
    {
        IEnumerable<Custom> GetAllCustoms();
    }
}
