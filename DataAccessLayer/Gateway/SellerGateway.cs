﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;

using DomainModel.Entities;

namespace DataAccessLayer.Gateway
{
    public class SellerGateway : ISellerGateway
    {
        private IDatabaseGateway _databaseGateway;

        public SellerGateway(IDatabaseGateway gateway)
        {
            _databaseGateway = gateway;
        }
        
        public SellerBonus CalculateSellerBonus(Seller seller)
        {
            using (IDbConnection connection = _databaseGateway.CreateConnection())
            {
                try
                {
                    connection.Open();

                    using (IDbCommand cmd = connection.CreateCommand())
                    {
                        string procedureName = "CALC_SELLER_BONUS";
                        cmd.CommandText = procedureName;
                        cmd.CommandType = CommandType.StoredProcedure;

                        IDbDataParameter paramSellerId = cmd.CreateParameter();
                        paramSellerId.ParameterName = "@id_seller";
                        paramSellerId.DbType = DbType.Int32;
                        paramSellerId.Value = seller.ID.Id;
                        cmd.Parameters.Add(paramSellerId);

                        decimal bonus = (decimal)cmd.ExecuteScalar();
                        return new SellerBonus(bonus);
                    }
                }
                catch (Exception e)
                {
                    throw new SellerGatewayException(this.GenerateExceptionText("Calculate seller bonus failed.", e), e);
                }
            }
        }

        public void AddSeller(Seller seller)
        {
            seller.ID = null;
            using (IDbConnection connection = _databaseGateway.CreateConnection())
            {
                try
                {
                    connection.Open();

                    using (IDbCommand cmd = connection.CreateCommand())
                    {
                        string cmdText = "INSERT INTO SELLER (NAME_SELLER, TOWN, PHONE, FIRED) " +
                            "VALUES (@seller_name, @seller_town, @seller_phone, @seller_fired);";
                        cmd.CommandText = cmdText;

                        IDbDataParameter paramSellerName = cmd.CreateParameter();
                        paramSellerName.ParameterName = "@seller_name";
                        paramSellerName.DbType = DbType.String;
                        paramSellerName.Value = seller.Name.FullName;
                        cmd.Parameters.Add(paramSellerName);

                        IDbDataParameter paramSellerTown = cmd.CreateParameter();
                        paramSellerTown.ParameterName = "@seller_town";
                        paramSellerTown.DbType = DbType.String;
                        paramSellerTown.Value = seller.Town.Name;
                        cmd.Parameters.Add(paramSellerTown);

                        IDbDataParameter paramSellerPhone = cmd.CreateParameter();
                        paramSellerPhone.ParameterName = "@seller_phone";
                        paramSellerPhone.DbType = DbType.String;
                        paramSellerPhone.Value = seller.Phone.Number;
                        cmd.Parameters.Add(paramSellerPhone);

                        IDbDataParameter paramSellerFired = cmd.CreateParameter();
                        paramSellerFired.ParameterName = "@seller_fired";
                        paramSellerFired.DbType = DbType.Int32;
                        paramSellerFired.Value = seller.FiredState.IsFired ? 1 : 0;
                        cmd.Parameters.Add(paramSellerFired);

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    throw new SellerGatewayException(this.GenerateExceptionText("Add seller failed.", e), e);
                }
            }
        }

        public void FireSeller(Seller seller)
        {

            using (IDbConnection connection = _databaseGateway.CreateConnection())
            {
                try
                {
                    connection.Open();

                    using (IDbCommand cmd = connection.CreateCommand())
                    {
                        string cmdText = "UPDATE SELLER SET FIRED = 1 WHERE ID_SELLER = @id_seller;";
                        cmd.CommandText = cmdText;

                        IDbDataParameter paramSellerId = cmd.CreateParameter();
                        paramSellerId.ParameterName = "@id_seller";
                        paramSellerId.DbType = DbType.Int32;
                        paramSellerId.Value = seller.ID.Id;
                        cmd.Parameters.Add(paramSellerId);

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    throw new SellerGatewayException(this.GenerateExceptionText("Fire seller failed.", e), e);
                }
            }
        }

        public IEnumerable<Seller> GetAllSellers()
        {
            string cmdText = "SELECT ID_SELLER, NAME_SELLER, TOWN, PHONE, FIRED FROM SELLER;";
            DataSet dataSet;
            try
            {
                dataSet = _databaseGateway.ExecuteCommand(cmdText);
            }
            catch (DatabaseGatewayException e)
            {
                throw new SellerGatewayException(this.GenerateExceptionText("Get working sellers failed.", e), e);
            }

            return this.GetSellersFromDataSet(dataSet);
        }

        public IEnumerable<Seller> GetWorkingSellers()
        {
            string cmdText = "SELECT ID_SELLER, NAME_SELLER, TOWN, PHONE, FIRED FROM SELLER WHERE FIRED = 0;";
            DataSet dataSet;
            try
            {
                dataSet = _databaseGateway.ExecuteCommand(cmdText);
            }
            catch (DatabaseGatewayException e)
            {
                throw new SellerGatewayException(this.GenerateExceptionText("Get working sellers failed.", e), e);
            }

            return this.GetSellersFromDataSet(dataSet);
        }

        private DataSet ExecuteSelect(string cmdText)
        {
            return _databaseGateway.ExecuteCommand(cmdText);
        }

        private IEnumerable<Seller> GetSellersFromDataSet(DataSet dataSet)
        {
            List<Seller> sellers = new List<Seller>();

            DataTable table = dataSet.Tables[0];

            foreach (DataRow row in table.Rows)
            {
                SellerId sellerID = new SellerId((int)row["ID_SELLER"]);
                Name sellerName = new Name(row["NAME_SELLER"] as string);
                Town sellerTown = new Town(row["TOWN"] as string);
                PhoneNumber sellerPhone = new PhoneNumber(row["PHONE"] as string);
                SellerFiredState sellerFiredState = new SellerFiredState((int)row["FIRED"] == 0 ? false : true);

                Seller seller = new Seller(sellerID, sellerName, sellerTown, sellerPhone, sellerFiredState);

                sellers.Add(seller);
            }

            return sellers.ToArray();
        }

        private string GenerateExceptionText(string text, Exception e)
        {
            return new StringBuilder()
                    .Append(text)
                    .AppendLine()
                    .Append(e.Message)
                    .ToString();
        }
    }
}
