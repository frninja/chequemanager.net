﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;

using DomainModel.Entities;

namespace DataAccessLayer.Gateway
{
    public class WarehouseGateway : IWarehouseGateway
    {
        private readonly IDatabaseGateway _databaseGateway;
 
        public WarehouseGateway(IDatabaseGateway gateway)
        {
            _databaseGateway = gateway;
        }

        public IEnumerable<Warehouse> GetAllWarehouses()
        {
            string cmdText = "SELECT ID_WH, NAIMEN, TOWN, TYPE_WH FROM WAREHOUSE;";

            DataSet dataSet;
            try
            {
                dataSet = this.ExecuteSelect(cmdText);
            }
            catch (Exception e)
            {
                throw new WarehouseGatewayException(this.GenerateExceptionText("Get warehouses failed", e), e);
            }

            return this.GetWarehousesFromDataSet(dataSet);
        }

        public IEnumerable<Warehouse> GetRetailWarehouses()
        {
            string cmdText = "SELECT ID_WH, NAIMEN, TOWN, TYPE_WH FROM WAREHOUSE WHERE TYPE_WH = 0;";

            DataSet dataSet;
            try
            {
                dataSet = this.ExecuteSelect(cmdText);
            }
            catch (Exception e)
            {
                throw new WarehouseGatewayException(this.GenerateExceptionText("Get retail warehouses failed", e), e);
            }

            return this.GetWarehousesFromDataSet(dataSet);
        }

        public IEnumerable<Warehouse> GetWholesaleWarehouses()
        {
            string cmdText = "SELECT ID_WH, NAIMEN, TOWN, TYPE_WH FROM WAREHOUSE WHERE TYPE_WH = 1;";

            DataSet dataSet;
            try
            {
                dataSet = this.ExecuteSelect(cmdText);
            }
            catch (Exception e)
            {
                throw new WarehouseGatewayException(this.GenerateExceptionText("Get wholesale warehouses failed", e), e);
            }

            return this.GetWarehousesFromDataSet(dataSet);
        }

        private DataSet ExecuteSelect(string cmdText)
        {
            return _databaseGateway.ExecuteCommand(cmdText);
        }

        private IEnumerable<Warehouse> GetWarehousesFromDataSet(DataSet dataSet)
        {
            List<Warehouse> warehouses = new List<Warehouse>();

            DataTable table = dataSet.Tables[0];

            foreach (DataRow row in table.Rows)
            {
                WarehouseId id = new WarehouseId((int)row["ID_WH"]);
                WarehouseName name = new WarehouseName(row["NAIMEN"] as string);
                Town town = new Town(row["TOWN"] as string);
                WarehouseType type = new WarehouseType((int)row["TYPE_WH"] == 0 ? WarehouseTypes.RETAIL : WarehouseTypes.WHOLESALE);

                Warehouse warehouse = new Warehouse(id, name, town, type);

                warehouses.Add(warehouse);
            }

            return warehouses.ToArray();
        }

        private string GenerateExceptionText(string text, Exception e)
        {
            return new StringBuilder()
                    .Append(text)
                    .AppendLine()
                    .Append(e.Message)
                    .ToString();
        }
    }
}
