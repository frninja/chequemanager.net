﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;

namespace DataAccessLayer.Gateway
{
    public interface IDatabaseGateway
    {
        //IDbConnection _currentTransactionConnection { get; }

        IDbConnection CreateConnection();

        void BeginTransaction(IsolationLevel level);
        void CommitActiveTransaction();
        void RollbackActiveTransaction();

        IDbCommand CreateCommandInActiveTransaction();
        IDbDataAdapter CreateDataAdapater();

        DataSet ExecuteCommand(string cmdText);
        DataSet ExecuteCommandInActiveTransaction(IDbCommand command);
        object ExecuteCommandScalarInActiveTransaction(IDbCommand command);
        int ExecuteCommandNonQueryInActiveTransaction(IDbCommand command);

        //DataSet ExecuteCommandInActiveTransaction(string cmdText, IDbTransaction transaction);
        object ExecuteStoredProcedure(string procedureName);
        object ExecuteStoredProcedure(string procedureName, IDbTransaction transaction);

    }
}
