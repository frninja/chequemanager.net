﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;

using DomainModel.Entities;

namespace DataAccessLayer.Gateway
{
    public class CustomGateway : ICustomGateway
    {
        private IDatabaseGateway _databaseGateway;

        public CustomGateway(IDatabaseGateway gateway)
        {
            _databaseGateway = gateway;
        }

        public IEnumerable<Custom> GetAllCustoms()
        {
            List<Custom> customs = new List<Custom>();

            string cmdText = "SELECT ID_TOVAR, NOMENCLATURE, RETAIL_PRICE, E_IZM FROM TOVAR;";

            DataSet customsDataSet = _databaseGateway.ExecuteCommand(cmdText);
            DataTable customsTable = customsDataSet.Tables[0];

            foreach (DataRow row in customsTable.Rows)
            {
                CustomId id = new CustomId((int)row["ID_TOVAR"]);
                CustomNomenclature nomenclature = new CustomNomenclature(row["NOMENCLATURE"] as string);
                CustomPrice price = new CustomPrice(row["RETAIL_PRICE"] as decimal?);
                UnitMeasure unitOfMeasure = new UnitMeasure(row["E_IZM"] as string);

                Custom custom = new Custom(id, nomenclature, price, unitOfMeasure);
                customs.Add(custom);
            }

            return customs.ToArray();
        }
    }
}
