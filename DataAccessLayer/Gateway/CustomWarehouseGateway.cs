﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;

using DomainModel.Entities;

namespace DataAccessLayer.Gateway
{
    public class CustomWarehouseGateway : ICustomWarehouseGateway
    {
        private readonly IDatabaseGateway _databaseGateway;

        public CustomWarehouseGateway(IDatabaseGateway gateway)
        {
            _databaseGateway = gateway;
        }

        public IEnumerable<CountedCustom> GetAllCustoms(Warehouse warehouse)
        {
            string cmdText = "SELECT t.ID_TOVAR, t.NOMENCLATURE, t.RETAIL_PRICE, t.E_IZM, twh.KOL " +
                "FROM TOVAR_WH twh JOIN TOVAR t ON twh.ID_TOVAR = t.ID_TOVAR " +
                "WHERE twh.ID_WH = @id_warehouse;";

            using (IDbConnection connection = _databaseGateway.CreateConnection())
            {
                try
                {
                    connection.Open();
                }
                catch (Exception e)
                {
                    throw new CustomWarehouseGatewayException(this.GenerateExceptionText("Get all customs failed to open connection.", e), e);
                }

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = cmdText;
                    IDbDataParameter paramWarehouseId = command.CreateParameter();
                    paramWarehouseId.ParameterName = "@id_warehouse";
                    paramWarehouseId.DbType = DbType.Int32;
                    paramWarehouseId.Value = warehouse.ID.Id;
                    command.Parameters.Add(paramWarehouseId);

                    
                    DataSet dataSet;
                    try
                    {
                       dataSet = ExecuteSelect(command);
                    }
                    catch (Exception e)
                    {
                        throw new CustomWarehouseGatewayException(this.GenerateExceptionText("Get all customs failed.", e), e);
                    }
                    return this.GetCountedCustomsFromDataSet(dataSet);
                }
            }
        }

        private DataSet ExecuteSelect(IDbCommand command)
        {
            DataSet dataSet = new DataSet();
            command.Prepare();
            IDbDataAdapter dataAdapter = _databaseGateway.CreateDataAdapater();
            dataAdapter.SelectCommand = command;

            int rowsCount = dataAdapter.Fill(dataSet);

            return dataSet;
        }

        private IEnumerable<CountedCustom> GetCountedCustomsFromDataSet(DataSet dataSet)
        {
            List<CountedCustom> customs = new List<CountedCustom>();

            DataTable table = dataSet.Tables[0];

            foreach (DataRow row in table.Rows)
            {
                CustomId id = new CustomId((int)row["ID_TOVAR"]);
                CustomNomenclature nomenclature = new CustomNomenclature(row["NOMENCLATURE"] as string);
                CustomPrice price = new CustomPrice((decimal)row["RETAIL_PRICE"]);
                UnitMeasure unitOfMeasure = new UnitMeasure(row["E_IZM"] as string);

                Custom custom = new Custom(id, nomenclature, price, unitOfMeasure);

                CustomCount count = new CustomCount((decimal)row["KOL"]);

                CountedCustom cc = new CountedCustom(custom, count);

                customs.Add(cc);
            }

            return customs.ToArray();
        }

        private string GenerateExceptionText(string text, Exception e)
        {
            return new StringBuilder()
                    .Append(text)
                    .AppendLine()
                    .Append(e.Message)
                    .ToString();
        }
    }
}
