﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;

using FirebirdSql.Data.FirebirdClient;


namespace DataAccessLayer.Gateway
{
    public class DatabaseGateway : IDatabaseGateway
    {
        // Only for transactions
        private IDbConnection _currentTransactionConnection;

        private IDbTransaction _currentTransaction;

        private string _connectionSettings;

        public DatabaseGateway()
        {
            FbConnectionStringBuilder connectionSettings = new FbConnectionStringBuilder();
            connectionSettings.DataSource = "class.mmcs.sfedu.ru";
            connectionSettings.Database = "/fbdata/newtest_podgr3.fdb";
            connectionSettings.Port = 3050;
            connectionSettings.UserID = "IT39";
            connectionSettings.Password = "it39";
            connectionSettings.Charset = "UTF8";

            connectionSettings.ConnectionTimeout = 3;

            _connectionSettings = connectionSettings.ToString();
        }

        public IDbConnection CreateConnection()
        {
            return new FbConnection(_connectionSettings);
        }

        public void BeginTransaction(IsolationLevel level)
        {
            if (_currentTransaction == null)
            {
                _currentTransactionConnection = this.CreateConnection();
                try
                {
                    _currentTransactionConnection.Open();
                }
                catch (Exception e)
                {
                    _currentTransactionConnection.Dispose();
                    throw new DatabaseGatewayException(string.Format("Begin transaction failed to open connection: {0}", e.Message), e);
                }

                try
                {
                    _currentTransaction = _currentTransactionConnection.BeginTransaction(level);
                }
                catch (Exception e)
                {
                    if (_currentTransaction != null)
                    {
                        _currentTransaction.Dispose();
                        _currentTransaction = null;
                    }
                    _currentTransactionConnection.Dispose();
                    _currentTransactionConnection = null;
                    throw new DatabaseGatewayException(string.Format("Begin transaction failed: {0}", e.Message), e);
                }
            }
            else
            {
                throw new DatabaseGatewayException("Begin transaction failed: There is active transaction. Parallel transactions forbidden.");
            }
           
        }

        public void CommitActiveTransaction()
        {
            if (_currentTransaction != null)
            {
                try
                {
                    _currentTransaction.Commit();
                }
                catch (Exception e)
                {
                    throw new DatabaseGatewayException(string.Format("Commit transaction failed: {0}", e.Message));
                }
                finally
                {
                    _currentTransaction.Dispose();
                    _currentTransactionConnection.Dispose();
                    _currentTransaction = null;
                    _currentTransactionConnection = null;
                }
            }
            else
            {
                throw new DatabaseGatewayException("Commit transcation failed: there is no active transcation.");
            }

        }

        public void RollbackActiveTransaction()
        {
            if (_currentTransaction != null)
            {
                try
                {
                    _currentTransaction.Rollback();
                }
                catch (Exception e)
                {
                    throw new DatabaseGatewayException(string.Format("Rollback transcation failed: {0}", e.Message), e);
                }
                finally
                {
                    _currentTransaction.Dispose();
                    _currentTransactionConnection.Dispose();
                    _currentTransaction = null;
                    _currentTransactionConnection = null;
                }
            }
            else
            {
                throw new DatabaseGatewayException("Rollback transcation failed: there is no active transaction");
            }

        }

        // Only for creating command in active transcation
        public IDbCommand CreateCommandInActiveTransaction()
        {
            if (_currentTransaction != null)
            {
                IDbCommand command = _currentTransactionConnection.CreateCommand();

                command.Transaction = _currentTransaction;

                return command;
            }
            else
            {
                throw new DatabaseGatewayException("Create command failed: there is no active transaction");
            }
        }

        public IDbDataAdapter CreateDataAdapater()
        {
            return new FbDataAdapter();
        }

        // Only for commands in active transaction
        public object ExecuteCommandScalarInActiveTransaction(IDbCommand command)
        {
            if (_currentTransaction != null)
            {
                try
                {
                    command.Prepare();
                    object result = command.ExecuteScalar();
                    return result;
                }
                catch (Exception e)
                {
                    throw new DatabaseGatewayException(string.Format("Execute command scalar failed: {0}", e.Message), e);
                }
            }
            else
            {
                throw new DatabaseGatewayException("Execute command scalar failed: there is no active transaction");
            }

        }

        // Only for commands in active transaction
        public int ExecuteCommandNonQueryInActiveTransaction(IDbCommand command)
        {
            if (_currentTransaction != null)
            {
                try
                {
                    command.Prepare();
                    return command.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    throw new DatabaseGatewayException(string.Format("Execute command non query failed: {0}", e.Message), e);
                }
            }
            else
            {
                throw new DatabaseGatewayException("Execute command non query failed: there is no active transaction");
            }
        }

        // Only for commands in active transaction
        public DataSet ExecuteCommandInActiveTransaction(IDbCommand command)
        {
            if (_currentTransaction != null)
            {
                DataSet dataSet = new DataSet();

                try
                {
                    command.Prepare();
                    IDbDataAdapter dataAdapter = new FbDataAdapter();
                    dataAdapter.SelectCommand = command;

                    int rowsCount = dataAdapter.Fill(dataSet);

                    return dataSet;
                }
                catch (Exception e)
                {
                    throw new DatabaseGatewayException(string.Format("Execute command failed: {0}", e.Message), e);
                }
            }
            else
            {
                throw new DatabaseGatewayException("Execute command failed: there is no active transaction");
            }

        }

        public DataSet ExecuteCommand(string cmdText)
        {
            using (IDbConnection connection = this.CreateConnection())
            {
                try
                {
                    connection.Open();
                }
                catch (Exception e)
                {
                    throw new DatabaseGatewayException(string.Format("Execute command failed: can't open connection - {0}", e.Message), e);
                }

                DataSet dataSet = new DataSet();

                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = cmdText;

                    try
                    {
                        command.Prepare();
                        IDbDataAdapter dataAdapter = new FbDataAdapter();
                        dataAdapter.SelectCommand = command;

                        int rowsCount = dataAdapter.Fill(dataSet);

                        return dataSet;
                    }
                    catch (Exception e)
                    {
                        throw new DatabaseGatewayException(string.Format("Execute command failed: {0}", e.Message), e);
                    }
                }
            }
        }

        public object ExecuteStoredProcedure(string procedureName)
        {
            using (IDbCommand command = _currentTransactionConnection.CreateCommand())
            {
                // !
                if (_currentTransaction != null)
                    command.Transaction = _currentTransaction;
                // !
                command.CommandText = procedureName;
                command.CommandType = CommandType.StoredProcedure;
                return command.ExecuteScalar();
            }

        }

        public object ExecuteStoredProcedure(string procedureName, IDbTransaction transaction)
        {
            using (IDbCommand command = _currentTransactionConnection.CreateCommand())
            {
                command.Transaction = transaction;
                command.CommandText = procedureName;
                command.CommandType = CommandType.StoredProcedure;

                return command.ExecuteScalar();
            }
        }
    }
}
